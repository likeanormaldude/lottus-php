/**
 * IMPORTANT - do not use imports in this file!
 * It will break global definition.
 */

declare namespace globalThis {
  declare const DEV_ENV: boolean;
  declare const $SessionManager: InstanceType<typeof SessionManager>;
  declare const $Elements: InstanceType<typeof Elements>;
  declare const $DBCaching: InstanceType<typeof DBCaching>;
  declare const ABS_HOME_URI: string;
  declare function getAbsHomeUri(): string;
  declare function isJsonString(string: string): boolean;
  declare function getRandomInt(min: number, max: number): number;
  declare function padLeft(nr: number, n: number, str = "0"): string;

  export interface Window {
    $Elements: InstanceType<typeof Elements>;
    $SessionManager: InstanceType<typeof SessionManager>;
    $DBCaching: InstanceType<typeof DBCaching>;
  }

  export interface JQuery {
    autotab: any;
  }

  export interface BodyRequestModal {
    serializedData?: ElementsSerializedData[];
  }

  export interface BodyRequestMiddleware {
    request:
      | "read"
      | "cacheGameData"
      | "checkCaching"
      | "getBasicGameInfo"
      | "updatePrecision";

    /**
     * @var {string} content - Required when 'request' is 'write'
     */
    content?: string;

    /**
     * @var {string} mode - Mode to open the file
     * @see https://www.php.net/manual/en/function.fopen.php
     */
    mode?: string;
  }

  export interface BasicGameInfo {
    game: string;
    numberOfCharacters: number;
    numberOfElements: number;
    range: {
      firstElem: string;
      lastElem: string;
    };
  }

  export interface AjaxExceptionResponse {
    responseText: BasicGameInfo;
  }

  export interface AjaxMiddlewareResponse {
    status: number;
    responseText: BasicGameInfo;
  }

  export interface ElementsSerializedData {
    componentTitle: string;
    sequenceSet: string[];
  }
}

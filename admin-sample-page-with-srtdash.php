<?php

require_once "inc/constants.php";
require_once ABSPATH.'/inc/autoload.php';
require_once ABSPATH .'/vendor/autoload.php';
require_once ABSPATH.'/inc/functions.php';

try{
    //Initializing objects
    $loader     = new Twig\Loader\FilesystemLoader();

    includeTwigPaths([
         ABSPATH.'/templates'
        ,ABSPATH.'/templates/sample-and-debug/'
        ,ABSPATH.'/templates/sample-and-debug/admin-sample-page-with-srtdash'
        ,ABSPATH.'/templates/sample-and-debug/srtdash-content-sample'
        ,ABSPATH.'/templates/page-areas/header'
        ,ABSPATH.'/templates/page-areas/sidebar'
        ,ABSPATH.'/templates/page-areas/offset-options'
        ,ABSPATH.'/templates/page-areas/footer'
    ], $loader);

    $twig       = new Twig\Environment($loader);
    $template   = $twig->load('admin-sample.html.twig');

    //Context array to be assigned on Twig.
    $context = [
         'ABSPATH' => ABSPATH
        ,'ABS_HOME_URI' => ABS_HOME_URI
        ,'show_breadcrumbs' => 0
        ,'title' => 'SRTDash Sample Page'
        ,'subtitle' => 'SRTDash Sample Page'
    ];

    //Rendering
    echo $template->render($context);
}catch( Exception $e ){
    die ('ERROR: ' . $e->getMessage());
}
# LOTTUS-PHP

## Description
Designed to more accurately play sorting games.


## Command line instructions

You can also upload existing files from your computer using the instructions below.

#### Git global setup
```sh
git config --global user.name "Your username"
git config --global user.email "your email"
```

#### Create a new repository
```sh
git clone https://gitlab.com/likeanormaldude/lottus.git
cd lottus
touch README.md
git add README.md
git commit -m "add README"
git push -u origin master
```

#### Push an existing folder
```sh
cd existing_folder
git init
git remote add origin https://gitlab.com/likeanormaldude/lottus.git
git add .
git commit -m "Initial commit"
git push -u origin master
```
#### Push an existing Git repository
```sh
cd existing_repo
git remote rename origin old-origin
git remote add origin https://gitlab.com/likeanormaldude/lottus.git
git push -u origin --all
git push -u origin --tags
```





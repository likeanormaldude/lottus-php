<?php

// Dft Imports
require_once "../inc/constants.php";
require_once ABSPATH.'/inc/autoload.php';
require_once ABSPATH .'/vendor/autoload.php';
require_once ABSPATH.'/inc/functions.php';

// Secondary Imports
require_once ABSPATH.'/misc/Lottus/SessionManager.php';

try {
    $status = 500; // Default server internal error.
    $responseText = NULL;

    $SessionManager = SessionManager::getInstance();

    /*if(DEV_ENV)
        setManualAppCachingConfig();*/

    $occurrences = $SessionManager->getOccurrences( SessionManager::RETURN_PADDED_STR );

    $sItems = $_POST['serializedData'];

    $basicGameInfo = $SessionManager->getBasicGameInfo();
    $args = array_merge( $basicGameInfo, [ 'numberOfComponents' => count($sItems) ] );
    $debug=1;

    $counter = 0;
    foreach( $sItems as $sItem ){
        $sequenceDetails = getSequenceSetDetails( $sItem['sequenceSet'], $occurrences );
        $args['args'][$counter] = [
             'componentTitle' => $sItem['componentTitle']
            ,'occurrences' => $sequenceDetails['padded_str']['occurrences']
            ,'elements' => $sequenceDetails['padded_str']['elements']
            ,'score' => $sequenceDetails['padded_str']['score']
        ];

        $counter++;
    }

    $cardElementComponent = Elements::getComponent( Elements::COMP_CARD_ELEMENTS, $args );

    $responseText = $cardElementComponent
        ->displayBottomLeft(TRUE)
        ->displayTopCenter( FALSE )
        ->makeComponent()
    ;

    // All good.
    $status=200;
}catch( Exception $e ){
    $responseText = $e->getMessage();
    DebugHTML::getInstance( $responseText )->debug();
}

$encodedResponse = json_encode([
    'status' => $status
    ,'responseText' => $responseText
]);

echo $encodedResponse;


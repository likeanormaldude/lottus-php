<?php

// --------------------------------------------------- EXCEPTIONS ------------------------------------------------------

define(
    'EXC_POST_REQUEST_MISSING'
    ,"'request' param must be set on POST"
);

define(
    'EXC_POST_FILEPATH_MISSING'
    ,"'filepath' param must be set on POST when the request isn't 'checkCaching'"
);

define(
    'EXC_POST_CONTENT_MISSING'
    ,"'content' param must be set on POST when the request is 'write'"
);

define(
    'EXC_POST_GAME_DATA'
    ,'Couldn\'t write content to'.CACHE_DIR.'/gameData/data.json'
);

// --------------------------------------------------- EXCEPTIONS ------------------------------------------------------







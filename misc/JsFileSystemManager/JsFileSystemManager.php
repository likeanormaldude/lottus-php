<?php

/**
 * Manages exclusively the filesystem. This class must be used whenever
 * a file or folder has to be manipulated
 * */
class JsFileSystemManager{

    /**
     * @var JsFileSystemManager $SingleTon
     * */
    private static $SingleTon;

    /**
     * @var string $filepath Filepath to handle the filestream.
     * */
    private $filepath;

    /**
     * @var string $mode $mode to handle the writing file stream.
     * */
    private $mode;

    public function __construct($filepath){
        $this->filepath = $filepath;
        $this->mode = ( ( $_POST['mode'] ) ? $_POST['mode'] : 'w' );
    }

    /**
     * @return self
     */
    public static function getInstance( $filepath ){
        if(!self::$SingleTon)//Guarantee just one instance
            self::$SingleTon = new JsFileSystemManager( $filepath );

        return self::$SingleTon;
    }

    /**
     * @param array $args
     * @param string $exc_msg
     * @return bool <p><b>TRUE</b> in case of successfully written file or <b>FALSE</b> in case of
     * any exception or error.</p> <p>Note that when the return is <b>FALSE</b>,
     * <i>$exc_msg</i> will be set with the exception message.</p>
     * */
    public function writeFile( $args, &$exc_msg = NULL ){

        if( !isset( $args['content'] ) ){
            $exc_msg = EXC_POST_CONTENT_MISSING;
            return FALSE;
        }

        $file = fopen($this->filepath, $this->mode);
        $write = fwrite($file, $args['content']);
        fclose($file);

        if( !$write ) {
            $exc_msg = "Couldn't write content to '".$this->filepath."'";
            return FALSE;
        }

        return TRUE;
    }

    /**
     * @param string $exc_msg
     * @return string|bool <p><b>TRUE</b> in case of read success or <b>FALSE</b> in case of
     * any exception or error.</p> <p>Note that when the return is <b>FALSE</b>,
     * <i>$exc_msg</i> will be set with the exception message.</p>
     * */
    public function readFile( &$exc_msg = NULL ){
        if( $c = file_get_contents( $this->filepath ) ){
            return $c;
        }else{
            $exc_msg = 'Couldn\'t read content of "'.$this->filepath.'"';
            return FALSE;
        }
    }

}
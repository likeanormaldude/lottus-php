<?php

// Dft Imports
require_once "../../inc/constants.php";
require_once ABSPATH.'/inc/autoload.php';
require_once ABSPATH .'/vendor/autoload.php';
require_once ABSPATH.'/inc/functions.php';

// Secondary Imports
require_once ABSPATH.'/misc/JsFileSystemManager/JsFileSystemManager.php';
require_once ABSPATH.'/misc/Lottus/SessionManager.php';
require_once "../cacheConstants.php";
require_once "../middlewareConstants.php";
require_once "../cacheFunctions.php";

/*
Possible POST params:
    - request: string;
    - content?: string; // Required in case of 'write' request
    - mode?: string;
    - filepath?: string;
*/

try{
    $status = 500; // Default server internal error.
    $responseText = NULL;

    if( !checkPOSTParams( $exc_msg_params ) )
        throw new Exception( $exc_msg_params );

    switch( $_POST['request'] ){
        case "cacheGameData":
            $feedCache = LottusCacheFactory::getInstance()->feedCache($exc_msg_feed_cache );

            if( $feedCache ) {
                $status = 200;
                $responseText = MSG_CACHE_GAME_DATA_SUCCESS;
            }else
                throw new Exception( $exc_msg_feed_cache );
            break;

        case "checkCaching":
            /*if( DEV_ENV )
                deleteDirectory(CACHE_DIR);*/

            if( SessionManager::getInstance()->initDefaultCacheFiles( $exc_msg_init_cache ) ){
                $status = 200;
                $responseText = MSG_CACHE_DEFAULT_STRUCTURE_CREATED;
            }else{
                throw new Exception( $exc_msg_init_cache );
            }
            break;

        case "updatePrecision":
            $SessionManager = SessionManager::getInstance();
            $SessionManager->setAppCachingProps([
                'precision' => 100
            ]);

            if( !$SessionManager->createPrecisionFile() )
                throw new Exception( EXC_PRECISION_CREATE );

            $status = 200;
            $responseText = "'precision' updated!";

            break;

        case "read":
            $fp = $_POST['filepath'];
            $read = JsFileSystemManager::getInstance( $fp )->readFile( $exc_msg_read_file );
            if( $read ){
                $status = 200;
                $responseText = $read;
            }else{
                throw new Exception( $exc_msg_read_file );
            }
            break;

        case "write":
            $fp = $_POST['filepath'];
            $write = JsFileSystemManager::getInstance( $fp )
                ->writeFile([ 'content' => $_POST['content'] ], $exc_msg_write_file);

            if( $write ){
                $status = 200;
                $responseText = "Content successfully written to '".$fp."'";
            }
            else
                throw new Exception( $exc_msg_write_file );
            break;

        case "getBasicGameInfo":
            $responseText = SessionManager::getInstance()->getBasicGameInfo();
            break;
    } // End request switch

}catch( Exception $e ){
//    if( DEV_ENV ){
//        // Delete cache dir
//        deleteDirectory( CACHE_DIR );
//    }
    $responseText = $e->getMessage();
    DebugHTML::getInstance( $responseText )->debug();
}

echo json_encode([
     'status' => $status
    ,'responseText' => $responseText
]);






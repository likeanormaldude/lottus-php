<?php

use TRegx\CleanRegex\Pattern;
require_once ABSPATH.'/misc/cacheConstants.php';

class SessionManager implements iSingleTon {

    const RETURN_ARRAY = 1;
    const RETURN_PADDED_STR = 2;

    /**
     * @var SessionManager $SingleTon
     * */
    private static $SingleTon;

    /**
     * Responsible for the object <i>'appCaching.json'</i>
     * @see https://gitlab.com/likeanormaldude/lottus-php/-/tree/dev/tmp
     * @var array $appCaching
     * */
    private $appCaching;

    /**
     * @var array $defaultCacheFilesAndFolders The default structure of <i>./cache</i>
     * */
    private $defaultCacheFilesAndFolders;

    /**
     * @var string $sManagerDftDir The absolute dir to SessionManager default files
     * */
    private $sManagerDftDir;

    /**
     * @var array $precision Object that holds a portion of <i>'./cache/gameData/data.json'</i>
     * */
    private $precision;

    /**
     * @var array $occurrences Matrix that counts each element according<p>
     * to <i>'precision'</i> elements (not the whole game
     * elements range)</p>
     * */
    private $occurrences;

    public function __construct(){

        $this->defaultCacheFilesAndFolders = [
            'dirs' => [
                 ABSPATH.'/cache'
                ,ABSPATH.'/cache/appCaching'
                ,ABSPATH.'/cache/appCaching/gameConfig'
                ,ABSPATH.'/cache/gameData'
            ],
            'files' => [
                 'cache.json' => ABSPATH.'/cache/cache.json'
                ,'appCaching.json' => ABSPATH.'/cache/appCaching/appCaching.json'
                ,'/gameConfig/megasena.json' => ABSPATH.'/cache/appCaching/gameConfig/megasena.json'
                ,'/gameData/data.json' => ABSPATH.'/cache/gameData/data.json'
            ]
        ];

        $this->sManagerDftDir = S_MANAGER_DEFAULTS;
        $this->appCaching = [];
        $this->precision = [];
        $this->occurrences = [];
    }

    /**
     * Get general game info based on the 'gameProfile' of <i>./cache/appCaching/appCaching.json</i>
     * and store it on $this->appCaching
     * @return array The $appCaching array
     * */
    public function getAppCachingConfig(){

        if( count( $this->appCaching ) > 0 ) return $this->appCaching;

        $c = file_get_contents( APP_CACHING_DIR.'/appCaching.json' );
        $appCaching = json_decode( $c, TRUE );

        // Valid associative array ( != NULL )
        if( isset( $appCaching ) ){
            $this->appCaching = $appCaching;
        }else
            throw new Exception( EXC_CACHE_APPCACHING_INVALID );

        return $appCaching;
    }

    /**
     * Checks if <i>precision.json</i> file is created and return its value, as
     * an array. <p> if the file doesn't exist, this method will create it
     * and automatically updates the <i>occurrences.json</i> file.
     * @throw Exception In case <i>data.json</i> is not created or
     * any other exception.
     * @return array The precision array
     * */
    public function getPrecision(){

        if( count( $this->precision ) > 0 ) return $this->precision;

        // Start verifications \\
        if( !file_exists( GAME_DATA_DIR.'/data.json' ) )
            throw new Exception( EXC_DATA_FILE_MISSING );
        // End verifications //

        if( !file_exists( PRECISION_FILEPATH ) )
            $this->createPrecisionFile();
        else{
            $c = file_get_contents( PRECISION_FILEPATH );
            $this->precision = json_decode( $c, TRUE );
        }

        return $this->precision;
    }

    /**
     * Get the game basic info according to <i>appCaching.json</i> file.
     * @return void|array
     * @throws Exception
     */
    public function getBasicGameInfo(){

        // In case it doesn't exist yet, the returned basic info will be
        // the 'megasena' game profile
        if( !file_exists( APP_CACHING_DIR.'/appCaching.json' ) ){
            $c = file_get_contents( S_MANAGER_DEFAULTS.'/gameConfig/megasena.json' );
            return json_decode( $c, TRUE );
        }

        $this->getAppCachingConfig();
        switch( $this->appCaching['gameProfile'] ){
            case MegaSenaCache::GAME_PROFILE:
                return MegaSenaCache::getInstance()->getBasicGameInfo();
        }
    }

    /**
     * @return array
     */
    public static function getCacheDirStructure(): array{
        return [
             CACHE_DIR.'/cache.json'
            ,CACHE_DIR.'/appCaching/appCaching.json'
            ,CACHE_DIR.'/appCaching/gameConfig/megasena.json'
            ,CACHE_DIR.'/gameData/data.json'
        ];
    }

    /**
     * @return self
     */
    public static function getInstance(){
        if(!self::$SingleTon)//Guarantee just one instance
            self::$SingleTon = new SessionManager();

        return self::$SingleTon;
    }

    /**
     * @param string $filepath
     * @return array
     * */
    private function _chkDefaultFolderAndFile( $filepath ){
        $f              = $filepath;
        $dir            = '/';

        $patternFile    = '[a-zA-Z]+\.json';
        $patternFolder  = '^\/(.+)\/';

        $pFile          = Pattern::of( $patternFile );
        $pFolder        = Pattern::of( $patternFolder );

        // File
        if( $pFile->test( $filepath ) ){
            $match = $pFile->match( $filepath );
            $rs = $match->all();
            $f = $rs[0];
        }

        // File
        if( $pFolder->test( $filepath ) ){
            $match = $pFolder->match( $filepath );
            $rs = $match->all();
            $dir = $rs[0];
        }

        $array = [
             'file'     => $f
            ,'folder'   => $dir
        ];

        return $array;
    }

    /**
     * Updates <i>appConfig.json</i> props. Therefore, the keys must be valid
     * @param array $propsArray The associative array containing the due
     * properties and the respective values
     * @throws Exception In case of any invalid property or some other Exception
     * @return self
     * */
    public function setAppCachingProps( array $propsArray ){
        // Start verifications \\
        if( !file_exists( APP_CACHING_DIR.'/appCaching.json' ) )
            throw new Exception( EXC_CACHE_APPCACHING_INVALID );

        if( count( $propsArray ) === 0 )
            throw new Exception( '$propsArray is empty.' );

        // End verifications //

        // Start props assign \\
        $appCaching = $this->getAppCachingConfig();
        $allowedKeys = array_keys( $appCaching );

        foreach( $propsArray as $prop => $value ){
            if( !in_array( $prop, $allowedKeys ) )
                throw new Exception( "\"$prop\" is not a valid appCaching key" );

            $appCaching[$prop] = $value;
        }

        $this->appCaching = $appCaching;
        // End props assign //

        // Updates the appCaching file
        $encoded    = json_encode( $appCaching, JSON_PRETTY_PRINT );
        $f          = fopen(APP_CACHING_DIR.'/appCaching.json', 'w');
        $write      = fwrite($f, $encoded);
        fclose( $f );

        if( $write === FALSE )
            throw new Exception("Couldn't write ".APP_CACHING_DIR."/appCaching.json" );

        return $this;
    }

    /**
     * Creates the <i>'precision'</i> file, used to get a portion of a large
     * <i>'./cache/gameData/data.json'</i> object.
     * @return bool <b>TRUE</b> in case of successfully created file or <b>FALSE</b>
     * on any error.
     * */
    public function createPrecisionFile(){
        // Gets the portion of data according to precision
        $LottusCache    = LottusCacheFactory::getInstance();
        $appCaching     = $this->getAppCachingConfig();
        $gameData       = $LottusCache->getGameData();
        $precision      = $appCaching['precision'];
        $length         = count( $gameData['contests'] );
        $offset         = ( ( $length <= 100 ) ? 0 : ( $length - $precision ) );

        $arrPrecision   = [
             'gameProfile' => $gameData['gameProfile']
            ,'contests' => array_slice( $gameData['contests'], $offset, $length )
        ];

        $this->precision = $arrPrecision;

        // Writes the file
        $encoded        = json_encode( $arrPrecision, JSON_PRETTY_PRINT );
        $f              = fopen(PRECISION_FILEPATH, 'w');
        $write          = fwrite($f, $encoded);
        fclose( $f );

        // Updates occurrences according to the fresh precision object
        $this->updateOccurrenceFile();

        return $write; // Weather is a successfully written or not
    }

    /**
     * Updates the <i>'./cache/gameData/occurrences.json'</i> file
     * <p>according to <i>'./cache/gameData/precision.json'</i></p>
     * @throws Exception;
     * @return self
     * */
    public function updateOccurrenceFile(){
        $p = $this->getPrecision();
        $countMatrix = [];

        foreach( $p['contests'] as $contest ){
            // Counts each element (occurrence)
            foreach( $contest['elements'] as $elem ){

                // To avoid undefined array index
                if( !isset( $countMatrix[$elem] ) ) $countMatrix[$elem] = 0;

                $countMatrix[$elem]++;
            }
        }

        // Sort asc keys
        ksort( $countMatrix );

        // Writes the file
        $encoded    = json_encode( $countMatrix, JSON_PRETTY_PRINT );
        $f          = fopen(OCCURRENCES_FILEPATH, 'w');
        $write      = fwrite($f, $encoded);
        fclose( $f );

        if(!$write)
            throw new Exception( "Couldn't write \"".OCCURRENCES_FILEPATH."\"" );

        return $this;
    }

    /**
     * @return array Array with the formatted string on each element
     * */
    private function _padOccurrencesAndFormat(){

        // Start verifications \\
        if( count( $this->occurrences ) === 0 )
            throw new Exception( '$this->occurrence is empty' );
        // End verifications //

        $r = [];

        foreach( $this->occurrences as $elem => $numberOfOcc ){
            $str = '('.str_pad( $numberOfOcc, 2, '0', STR_PAD_LEFT ).'x)';
            
            $r[$elem] = $str;
        }

        return $r;
    }

    /**
     * Gets what's store on <i>'./cache/gameData/occurrences.json'</i>
     * @return array The occurrences matrix or the formated occurences, depending of
     * $occReturnType
     * <code>[ elem => number ]</code>
     * */
    public function getOccurrences( $occReturnType = self::RETURN_ARRAY ){
        if( count( $this->occurrences ) > 0 ) return $this->occurrences;

        // Start verifications \\
        if( !file_exists( PRECISION_FILEPATH ) )
            throw new Exception( EXC_PRECISION_MISSING );
        // End verifications //

        $c = file_get_contents( OCCURRENCES_FILEPATH );
        $occurrences = json_decode( $c, TRUE );

        // Valid associative array ( != NULL )
        if( isset( $occurrences ) ){
            $this->occurrences = $occurrences;
        }else
            throw new Exception( EXC_OCCURRENCES_INVALID );

        $r = (
            ( $occReturnType === self::RETURN_ARRAY )
                ? $occurrences
                : $this->_padOccurrencesAndFormat()
        );

        return $r;
    }

    /**
     * Check the default files and folders and writes them, if necessary
     * default to <i>'./cache'</i>
     * @param string &$exc_msg = NULL
     * @return bool <p>
     * <b>TRUE</b> in case of all <i>Lottus Cache</i> files and folders are
     * successfully checked/created or <b>FALSE</b> otherwise. Note that when
     * the return is <b>FALSE</b>, <i>$exc_msg</i> will be set with the
     * exception message.</p>
     * */
    public function initDefaultCacheFiles( &$exc_msg = NULL ){
        $feedCache = FALSE;

        // Check/create all default directories
        foreach( $this->defaultCacheFilesAndFolders['dirs'] as $dir ){
            if( !file_exists( $dir ) )
                mkdir($dir);
        }

        // Check/create all default files
        foreach( $this->defaultCacheFilesAndFolders['files'] as $dftFolderAndFile => $filepath ){

            if( !file_exists( $filepath ) ){
                $check = $this->_chkDefaultFolderAndFile( $dftFolderAndFile );
                /*
                  Check if should feed cache of the default game profile, in case it is not set
                  (see 'if' condition)
                 */
                if( $check['folder'] === '/gameData/' && $check['file'] === 'data.json' )
                    $feedCache = TRUE;

                $filepathFrom   = $this->sManagerDftDir.$check['folder'].$check['file'];

                $fileCopied = copy( $filepathFrom, $filepath );

                if( $fileCopied === FALSE ){
                    $exc_msg = "Couldn't write ".$filepath;
                    return FALSE;
                }

            }
        }

        // Check if it's needed to write the local cache
        if( $feedCache ){
            $LottusCache = LottusCacheFactory::getInstance();

            // Feed default cache
            $fc = $LottusCache->feedCache( $exc_msg_feed_cache );

            if( !$fc ){
                $exc_msg = $exc_msg_feed_cache;
                return FALSE;
            }

            // In a fresh cache writing, 'precision' file has to be created.
            if( !$this->createPrecisionFile() ){
                $exc_msg = EXC_PRECISION_CREATE;
                return FALSE;
            }
        }

        // All good.
        return TRUE;
    }

}// End of class
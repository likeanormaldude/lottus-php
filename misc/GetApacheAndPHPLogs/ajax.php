<?php

require_once "../../inc/constants.php";
require_once ABSPATH.'/inc/functions.php';
require_once 'GetApacheAndPHPLogs.php';

$GetApacheAndPHPLogs = new GetApacheAndPHPLogs();

$_POST['request'] = 'getServerLogs';
$_POST['preTag'] = 1;

switch( $_POST['request'] ){
    case "getServerLogs":
        echo $GetApacheAndPHPLogs->getPHPErrorLog( $_POST['preTag'] );
        break;
}



<?php

/**
 * Check if all files and folder are set according to Lottus App Cache requirements
 * @param string $exc_msg
 * */
function checkCacheStructure( &$exc_msg = NULL ){
    $dirStructure = SessionManager::getCacheDirStructure();
    $isStructureOk = TRUE;

    foreach( $dirStructure as $filepath ){
        if( !file_exists( $filepath ) ){
            $exc_msg = '"'.$filepath.'"must be created before trying to cache game data.';
            $isStructureOk = FALSE;
            break;
        }
    }

    return $isStructureOk;
}

/**
 * @param string $exc_msg
 * */
function checkPOSTParams( &$exc_msg = NULL ){
    if( !isset( $_POST['request'] ) ){
        $exc_msg = EXC_POST_REQUEST_MISSING;
        return FALSE;
    }

    $isFilePathMissing = ( in_array( $_POST['request'], [ 'read', 'write' ] ) && !isset( $_POST['filepath'] ) );

    if( $isFilePathMissing ){
        $exc_msg = EXC_POST_FILEPATH_MISSING;
        return FALSE;
    }

    // All good.
    return TRUE;
}
<?php

// ----------------------------------------------- GENERAL CONSTANTS ---------------------------------------------------

define(
    'MEGA_SENA_BASE_URI'
    ,'https://us-central1-mega-sena-818a2.cloudfunctions.net/megasenaApi'
);

define(
    'S_MANAGER_DEFAULTS'
    ,ABSPATH.'/misc/Lottus/SessionManagerDefaults'
);

define(
    'CACHE_DIR'
    ,ABSPATH.'/cache'
);

define(
    'APP_CACHING_DIR'
    ,CACHE_DIR.'/appCaching'
);

define(
    'GAME_CONFIG_DIR'
    ,CACHE_DIR.'/appCaching/gameConfig'
);

define(
    'GAME_DATA_DIR'
    ,CACHE_DIR.'/gameData'
);

define(
    'PRECISION_FILEPATH'
    ,GAME_DATA_DIR.'/precision.json'
);

define(
    'OCCURRENCES_FILEPATH'
    ,GAME_DATA_DIR.'/occurrences.json'
);


// ----------------------------------------------- GENERAL CONSTANTS ---------------------------------------------------

// --------------------------------------------------- MESSAGES --------------------------------------------------------

define(
     'MSG_CACHE_GAME_DATA_SUCCESS'
    ,'Cache created at "'.GAME_DATA_DIR.'/data.json'.'"!'
);

define(
     'MSG_CACHE_DEFAULT_STRUCTURE_CREATED'
    ,'App cache default files and folders created at ./cache'
);

// --------------------------------------------------- MESSAGES --------------------------------------------------------

// --------------------------------------------------- EXCEPTIONS ------------------------------------------------------

define(
    'EXC_CACHE_APPCACHING_MISSING'
    ,'"'.APP_CACHING_DIR.'/appCaching.json" must be created before trying to cache game data.'
);

define(
    'EXC_CACHE_APPCACHING_INVALID'
    ,'"'.APP_CACHING_DIR.'/appCaching.json'.'" is invalid or empty'
);

define(
    'EXC_CACHE_MEGASENA_CONFIG_INVALID'
    ,'"'.GAME_CONFIG_DIR.'/megasena.json" is invalid or empty'
);

define(
    'EXC_CACHE_CHECKING'
    ,'Error at cache checking.'
);

define(
    'EXC_PRECISION_CREATE'
    ,"Couldn't create \"".PRECISION_FILEPATH."\""
);

define(
    'EXC_DATA_FILE_MISSING'
    ,'"'.GAME_DATA_DIR.'/data.json" must be created.'
);

define(
    'EXC_OCCURRENCES_INVALID'
    ,'"'.GAME_DATA_DIR.'/occurences.json" is invalid or empty'
);

define(
    'EXC_PRECISION_MISSING'
    ,'"'.PRECISION_FILEPATH.'" is invalid or doesn\'t exist'
);




// --------------------------------------------------- EXCEPTIONS ------------------------------------------------------







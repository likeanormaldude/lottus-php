jQuery.validator.setDefaults({
    errorElement: 'span',
    // errorClass: 'text-danger',
    //it specifies the error message display position
    errorPlacement: function (error, element) {
        // $(error).insertAfter($(element).siblings(".validation-label-msg"));
        error.addClass('text-danger');
        $(element).parent().append($(error));
    }
});

// Default Messages
// Source: https://stackoverflow.com/questions/2457032/jquery-validation-change-default-error-message/2457053

jQuery.extend(jQuery.validator.messages, {
    required: "Campo obrigatório.",
    remote: "Please fix this field.",
    email: "Informe um e-mail válido.",
    url: "Informe uma URL válida.",
    date: "Please enter a valid date.",
    dateISO: "Please enter a valid date (ISO).",
    number: "Informe somente números.",
    digits: "Informe somente dígitos.",
    creditcard: "Please enter a valid credit card number.",
    equalTo: "Please enter the same value again.",
    accept: "Please enter a value with a valid extension.",
    maxlength: jQuery.validator.format("Limite de {0} caracteres."),
    minlength: jQuery.validator.format("Informe pelo menos {0} caracteres."),
    rangelength: jQuery.validator.format("Please enter a value between {0} and {1} characters long."),
    range: jQuery.validator.format("Please enter a value between {0} and {1}."),
    max: jQuery.validator.format("Informe um valor menor ou igual a {0}."),
    min: jQuery.validator.format("Informe um valor maior ou igual a {0}.")
});

$(async function () {
  const rs = await SessionManager.getBasicGameInfo();
  const basicInfo = rs.responseText;
  const numOfElements = basicInfo.numberOfElements;

  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  const debug = 1;

  /* Start clear Icon Event Listeners  */

  $("#icon-clear-elem1").on("click", () => {
    $(".frame-element[aria-rowindex='frame-row-1']").val("");
    $(`#fr-elem1`).focus();
  });

  $("#icon-clear-elem2").on("click", () => {
    $(".frame-element[aria-rowindex='frame-row-2']").val("");
    $(`#fr-elem${numOfElements + 1}`).focus();
  });

  $("#icon-clear-elem3").on("click", () => {
    $(".frame-element[aria-rowindex='frame-row-3']").val("");
    $(`#fr-elem${numOfElements * 2 + 1}`).focus();
  });

  $("#icon-clear-elem4").on("click", () => {
    $(".frame-element[aria-rowindex='frame-row-4']").val("");
    $(`#fr-elem${numOfElements * 3 + 1}`).focus();
  });

  $("#icon-clear-elem5").on("click", () => {
    $(".frame-element[aria-rowindex='frame-row-5']").val("");
    $(`#fr-elem${numOfElements * 4 + 1}`).focus();
  });

  $("#icon-clear-elem6").on("click", () => {
    $(".frame-element[aria-rowindex='frame-row-6']").val("");
    $(`#fr-elem${numOfElements * 5 + 1}`).focus();
  });

  $("#icon-clear-elem7").on("click", () => {
    $(".frame-element[aria-rowindex='frame-row-7']").val("");
    $(`#fr-elem${numOfElements * 6 + 1}`).focus();
  });

  $("#icon-clear-elem8").on("click", () => {
    $(".frame-element[aria-rowindex='frame-row-8']").val("");
    $(`#fr-elem${numOfElements * 7 + 1}`).focus();
  });

  $("#icon-clear-elem9").on("click", () => {
    $(".frame-element[aria-rowindex='frame-row-9']").val("");
    $(`#fr-elem${numOfElements * 8 + 1}`).focus();
  });

  $("#icon-clear-elem10").on("click", () => {
    $(".frame-element[aria-rowindex='frame-row-10']").val("");
    $(`#fr-elem${numOfElements * 9 + 1}`).focus();
  });

  $("#icon-clear-elem11").on("click", () => {
    $(".frame-element[aria-rowindex='frame-row-11']").val("");
    $(`#fr-elem${numOfElements * 10 + 1}`).focus();
  });

  $("#icon-clear-elem12").on("click", () => {
    $(".frame-element[aria-rowindex='frame-row-12']").val("");
    $(`#fr-elem${numOfElements * 11 + 1}`).focus();
  });

  /* End clear Icon Event Listeners */

  class Elements {
    private static _instance: Elements;
    private serializedData: ElementsSerializedData[];

    constructor() {
      this.serializedData = [];
    }

    public static get Instance() {
      return this._instance || (this._instance = new this());
    }

    public serialize(): Array<ElementsSerializedData> {
      let counter = 0;
      let rowCounter = 0;
      let rowChange: boolean;

      // Reset the serialize variable
      this.serializedData = [];

      $(".frame-element").each((_k, elem) => {
        if (typeof this.serializedData[rowCounter] === "undefined") {
          // @ts-ignore
          this.serializedData[rowCounter] = {};
          this.serializedData[rowCounter].sequenceSet = [];
        }

        this.serializedData[rowCounter];
        // @ts-ignore
        this.serializedData[rowCounter].sequenceSet.push($(elem).val());

        rowChange = counter + 1 === numOfElements && numOfElements > 1;
        if (rowChange) {
          this.serializedData[rowCounter].componentTitle = $(
            `#frames-title${rowCounter + 1}`
          ).text();
          rowCounter++;
          counter = 0;
        } else {
          counter++;
        }
      });
      return this.serializedData;
    }

    public copyElementsToClipboard(rowNumber: number) {
      const html: string[] = [];
      $(`#row-elements${rowNumber} td`).each((k, tdElem) => {
        // console.log($(tdElem).html());
        html.push($(tdElem).html());
      });

      // @ts-ignore
      copyToClipboard(html.join(" "), { message: "teste" });
    }
  }

  window.$Elements = Elements.Instance;
});

"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
$(function () {
    return __awaiter(this, void 0, void 0, function* () {
        const rs = yield SessionManager.getBasicGameInfo();
        const basicInfo = rs.responseText;
        const numOfElements = basicInfo.numberOfElements;
        const debug = 1;
        $("#icon-clear-elem1").on("click", () => {
            $(".frame-element[aria-rowindex='frame-row-1']").val("");
            $(`#fr-elem1`).focus();
        });
        $("#icon-clear-elem2").on("click", () => {
            $(".frame-element[aria-rowindex='frame-row-2']").val("");
            $(`#fr-elem${numOfElements + 1}`).focus();
        });
        $("#icon-clear-elem3").on("click", () => {
            $(".frame-element[aria-rowindex='frame-row-3']").val("");
            $(`#fr-elem${numOfElements * 2 + 1}`).focus();
        });
        $("#icon-clear-elem4").on("click", () => {
            $(".frame-element[aria-rowindex='frame-row-4']").val("");
            $(`#fr-elem${numOfElements * 3 + 1}`).focus();
        });
        $("#icon-clear-elem5").on("click", () => {
            $(".frame-element[aria-rowindex='frame-row-5']").val("");
            $(`#fr-elem${numOfElements * 4 + 1}`).focus();
        });
        $("#icon-clear-elem6").on("click", () => {
            $(".frame-element[aria-rowindex='frame-row-6']").val("");
            $(`#fr-elem${numOfElements * 5 + 1}`).focus();
        });
        $("#icon-clear-elem7").on("click", () => {
            $(".frame-element[aria-rowindex='frame-row-7']").val("");
            $(`#fr-elem${numOfElements * 6 + 1}`).focus();
        });
        $("#icon-clear-elem8").on("click", () => {
            $(".frame-element[aria-rowindex='frame-row-8']").val("");
            $(`#fr-elem${numOfElements * 7 + 1}`).focus();
        });
        $("#icon-clear-elem9").on("click", () => {
            $(".frame-element[aria-rowindex='frame-row-9']").val("");
            $(`#fr-elem${numOfElements * 8 + 1}`).focus();
        });
        $("#icon-clear-elem10").on("click", () => {
            $(".frame-element[aria-rowindex='frame-row-10']").val("");
            $(`#fr-elem${numOfElements * 9 + 1}`).focus();
        });
        $("#icon-clear-elem11").on("click", () => {
            $(".frame-element[aria-rowindex='frame-row-11']").val("");
            $(`#fr-elem${numOfElements * 10 + 1}`).focus();
        });
        $("#icon-clear-elem12").on("click", () => {
            $(".frame-element[aria-rowindex='frame-row-12']").val("");
            $(`#fr-elem${numOfElements * 11 + 1}`).focus();
        });
        class Elements {
            constructor() {
                this.serializedData = [];
            }
            static get Instance() {
                return this._instance || (this._instance = new this());
            }
            serialize() {
                let counter = 0;
                let rowCounter = 0;
                let rowChange;
                this.serializedData = [];
                $(".frame-element").each((_k, elem) => {
                    if (typeof this.serializedData[rowCounter] === "undefined") {
                        this.serializedData[rowCounter] = {};
                        this.serializedData[rowCounter].sequenceSet = [];
                    }
                    this.serializedData[rowCounter];
                    this.serializedData[rowCounter].sequenceSet.push($(elem).val());
                    rowChange = counter + 1 === numOfElements && numOfElements > 1;
                    if (rowChange) {
                        this.serializedData[rowCounter].componentTitle = $(`#frames-title${rowCounter + 1}`).text();
                        rowCounter++;
                        counter = 0;
                    }
                    else {
                        counter++;
                    }
                });
                return this.serializedData;
            }
            copyElementsToClipboard(rowNumber) {
                const html = [];
                $(`#row-elements${rowNumber} td`).each((k, tdElem) => {
                    html.push($(tdElem).html());
                });
                copyToClipboard(html.join(" "), { message: "teste" });
            }
        }
        window.$Elements = Elements.Instance;
    });
});
//# sourceMappingURL=Elements.js.map
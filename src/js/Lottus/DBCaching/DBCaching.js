"use strict";
class DBCaching {
    constructor(log = false) {
        this.log = log;
        this.SessionManager = new SessionManager(true);
    }
    get getSessionManager() {
        return this.SessionManager;
    }
    _log(msg, parseJson = false) {
        if (this.log) {
            if (parseJson)
                msg = JSON.parse(msg);
            console.log(msg);
        }
    }
    init() {
        this.SessionManager.run();
        return this;
    }
    writeCache() {
        this._log("Writing game data cache...");
        const data = {
            request: "cacheGameData"
        };
        $.ajax({
            url: `${ABS_HOME_URI}/misc/JsFileSystemManager/middleware.php`,
            type: "POST",
            data,
            success: (rs) => {
                this._log(rs, isJsonString(rs));
            },
            error: function (e) {
                console.log(e.responseText);
            }
        });
    }
    readCache() {
        console.log("aa");
    }
}
window.$DBCaching = new DBCaching(DEV_ENV);
//# sourceMappingURL=DBCaching.js.map
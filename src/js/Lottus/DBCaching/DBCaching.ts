/*
A reminder: to manipulate the filesystem, use a backend programming language;
*/

class DBCaching {
  private SessionManager: SessionManager;

  /**
   * @param {boolean} log - Weather this class should output its log or not
   */
  constructor(private log: boolean = false) {
    this.SessionManager = new SessionManager(true);
  }

  public get getSessionManager() {
    return this.SessionManager;
  }

  private _log(msg: string, parseJson = false) {
    if (this.log) {
      if (parseJson) msg = JSON.parse(msg);
      console.log(msg);
    }
  }

  init() {
    // Run SessionManager
    this.SessionManager.run();
    // this.writeCache();

    return this;
  }

  writeCache(): void {
    this._log("Writing game data cache...");

    const data: BodyRequestMiddleware = {
      request: "cacheGameData"
    };

    $.ajax({
      url: `${ABS_HOME_URI}/misc/JsFileSystemManager/middleware.php`,
      type: "POST",
      data,
      success: (rs) => {
        this._log(rs, isJsonString(rs));
      },
      error: function (e) {
        console.log(e.responseText);
      }
    });
  }

  readCache(): void {
    console.log("aa");
  }
}

window.$DBCaching = new DBCaching(DEV_ENV);

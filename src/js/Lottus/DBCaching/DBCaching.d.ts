declare class DBCaching {
    private log;
    private SessionManager;
    constructor(log?: boolean);
    get getSessionManager(): SessionManager;
    private _log;
    init(): this;
    writeCache(): void;
    readCache(): void;
}

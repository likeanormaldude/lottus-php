/*
- Check existing cache config
- Create all folders and files needed
- Check current caching
- If empty, set default 'mega-sena' with the known data
*/

// eslint-disable-next-line @typescript-eslint/no-unused-vars
class SessionManager {
  private static _instance: SessionManager;

  /**
   * @param {boolean} log - Weather this class should output its log or not
   */
  constructor(private log: boolean = false) {}

  private _log(msg: string, parseJson = false) {
    if (this.log) {
      if (parseJson) msg = JSON.parse(msg);
      console.log(msg);
    }
  }

  public static get Instance() {
    return this._instance || (this._instance = new this());
  }

  run(): void {
    this._log("Running Session Manager...");
    this.checkLocalCaching();
    // this.updatePrecision();
  }

  public static async getBasicGameInfo(): Promise<
    AjaxMiddlewareResponse | AjaxExceptionResponse
  > {
    const data: BodyRequestMiddleware = {
      request: "getBasicGameInfo"
    };

    const response:
      | AjaxMiddlewareResponse
      | AjaxExceptionResponse = await new Promise((res, rej) => {
      $.ajax({
        url: `${ABS_HOME_URI}/misc/JsFileSystemManager/middleware.php`,
        type: "POST",
        dataType: "json",
        data,
        success: function (rs) {
          res(rs);
        },
        error: function (e) {
          rej(e.responseText);
        }
      });
    });

    return response;
  }

  checkLocalCaching() {
    this._log("Checking local caching...");
    const data: BodyRequestMiddleware = {
      request: "checkCaching"
    };

    $.ajax({
      url: `${ABS_HOME_URI}/misc/JsFileSystemManager/middleware.php`,
      type: "POST",
      data,
      success: (rs) => {
        this._log(rs, isJsonString(rs));
      },
      error: function (e) {
        console.log(e.responseText);
      }
    });
  }

  updatePrecision() {
    this._log("Updating precision...");
    const data: BodyRequestMiddleware = {
      request: "updatePrecision"
    };

    $.ajax({
      url: `${ABS_HOME_URI}/misc/JsFileSystemManager/middleware.php`,
      type: "POST",
      data,
      success: (rs) => {
        this._log(rs, isJsonString(rs));
      },
      error: function (e) {
        console.log(e.responseText);
      }
    });
  }
}

window.$SessionManager = SessionManager.Instance;

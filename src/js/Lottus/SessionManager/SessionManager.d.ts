declare class SessionManager {
    private log;
    private static _instance;
    constructor(log?: boolean);
    private _log;
    static get Instance(): SessionManager;
    run(): void;
    static getBasicGameInfo(): Promise<AjaxMiddlewareResponse | AjaxExceptionResponse>;
    checkLocalCaching(): void;
    updatePrecision(): void;
}

"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
class SessionManager {
    constructor(log = false) {
        this.log = log;
    }
    _log(msg, parseJson = false) {
        if (this.log) {
            if (parseJson)
                msg = JSON.parse(msg);
            console.log(msg);
        }
    }
    static get Instance() {
        return this._instance || (this._instance = new this());
    }
    run() {
        this._log("Running Session Manager...");
        this.checkLocalCaching();
    }
    static getBasicGameInfo() {
        return __awaiter(this, void 0, void 0, function* () {
            const data = {
                request: "getBasicGameInfo"
            };
            const response = yield new Promise((res, rej) => {
                $.ajax({
                    url: `${ABS_HOME_URI}/misc/JsFileSystemManager/middleware.php`,
                    type: "POST",
                    dataType: "json",
                    data,
                    success: function (rs) {
                        res(rs);
                    },
                    error: function (e) {
                        rej(e.responseText);
                    }
                });
            });
            return response;
        });
    }
    checkLocalCaching() {
        this._log("Checking local caching...");
        const data = {
            request: "checkCaching"
        };
        $.ajax({
            url: `${ABS_HOME_URI}/misc/JsFileSystemManager/middleware.php`,
            type: "POST",
            data,
            success: (rs) => {
                this._log(rs, isJsonString(rs));
            },
            error: function (e) {
                console.log(e.responseText);
            }
        });
    }
    updatePrecision() {
        this._log("Updating precision...");
        const data = {
            request: "updatePrecision"
        };
        $.ajax({
            url: `${ABS_HOME_URI}/misc/JsFileSystemManager/middleware.php`,
            type: "POST",
            data,
            success: (rs) => {
                this._log(rs, isJsonString(rs));
            },
            error: function (e) {
                console.log(e.responseText);
            }
        });
    }
}
window.$SessionManager = SessionManager.Instance;
//# sourceMappingURL=SessionManager.js.map
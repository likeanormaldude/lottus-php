/**
 * @return {string}
 */
function getAbsHomeUri() {
  const origin = `${location.origin}/`; // "h ttp://localhost:8080/"
  const pn = location.pathname.split("/"); // let s = "/lottus-php/admin/formAnalysis.php";
  const appname = pn[1];

  return origin + appname;
}

/**
 * @param {string} str
 * @return {boolean}
 */
function isJsonString(str) {
  try {
    JSON.parse(str);
  } catch (e) {
    return false;
  }
  return true;
}

/**
 * @param {string} msg - Modal content
 * @param {number} bBoxType - <p>
 *     &nbsp;&nbsp;&nbsp;1 - alert (with ok button)
 *     &nbsp;&nbsp;&nbsp;2 - dialog
 *</p>
 * @param {number} responseType - 1 or 200 - <i>Success</i>, 2 or 500 - <i>error</i> or 3 <i>warning</i>
 * @param {object} options - Any further option to bBox plugin
 */
function _alert(msg, bBoxType = 1, responseType = 1, options = {}) {
  let bBox, msgClass, iconClass;

  if (responseType === 1 || responseType === 200) {
    msgClass = "success-bBox";
    iconClass = "bBox-icon success-bBox imoon-check";
  } else if (responseType === 2 || responseType === 500) {
    msgClass = "error-bBox";
    iconClass = "bBox-icon error-bBox imoon-error";
  } else if (responseType === 3) {
    msgClass = "warning-bBox";
    iconClass = "bBox-icon warning-bBox imoon-warning";
  }

  if (options.hasOwnProperty("modalIcon") && options.modalIcon === false)
    iconClass += " d-none";

  const message = [
    '<div class="row">',
    '    <div class="col-1 pl-0">',
    '        <i class="' + iconClass + '"></i>',
    "    </div>",
    '    <div class="col-11 pl-0 text-center bBox-content">',
    '        <p class="' + msgClass + '">' + msg + "</p>",
    "    </div>",
    "</div>"
  ].join("");

  bBoxOptions = {
    message: message,
    backdrop: true,
    ...options
  };

  if (bBoxType === 1) {
    bBox = bootbox.alert(bBoxOptions);
  } else if (bBoxType === 2) {
    bBox = bootbox.dialog(bBoxOptions);
  }

  return bBox;
}

function padLeft(nr, n, str) {
  return Array(n - String(nr).length + 1).join(str || "0") + nr;
}

/**
 * Returns a random integer between min (inclusive) and max (inclusive).
 * The value is no lower than min (or the next integer greater than min
 * if min isn't an integer) and no greater than max (or the next integer
 * lower than max if max isn't an integer).
 * Using Math.round() will give you a non-uniform distribution!
 */
function getRandomInt(min, max) {
  min = Math.ceil(min);
  max = Math.floor(max);
  return Math.floor(Math.random() * (max - min + 1)) + min;
}

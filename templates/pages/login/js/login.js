$(function(){



    $("form").validate({
        rules: {
            usuario: {
                required: true,
                normalizer: function(value) {
                    return $.trim(value);
                }
            },
            senha: {
                // required: true,
                normalizer: function(value) {
                    return $.trim(value);
                }
            },
        },
        messages : {
            /*usuario : {
                required : 'Informe o usuário'
            },*/
            senha : {
                // required : 'Informe a senha'
            },
        },
        submitHandler: function (form) {

            $(form).ajaxSubmit({
                // url: 'ajax/salvarAvaliacao.php',
                type: 'post',
                beforeSubmit: function () {
                    $('#btn-login').attr('disabled', true);
                },
                success: function (r) {
                    const rs = JSON.parse(r);
                    if(DEV_ENV) console.log(r);

                    if( rs.status !== 200 ){
                        const bBox = _alert(rs.responseText, 2, rs.status);
                        bBox.init(function(){
                            setTimeout(function(){
                                bootbox.hideAll();
                                $("#usuario").focus();
                            }, 900);
                        });
                    }else{
                        location = `${ABS_HOME_URI}/admin/index.php`;
                    }

                    $('#btn-login').attr('disabled', false);
                }
            });

        }
    });

});
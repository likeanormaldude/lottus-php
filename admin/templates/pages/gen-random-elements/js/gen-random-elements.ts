$(async function () {
  const rs = await SessionManager.getBasicGameInfo();
  const bInfo = rs.responseText;

  $("#btn-gen-random-elements").on("click", () => {
    const range: {
      min: number;
      max: number;
    } = {
      min: parseInt(bInfo.range.firstElem),
      max: parseInt(bInfo.range.lastElem)
    };

    let n: number;
    const arrN: number[] = [];
    let counter = 6;
    let i = 0;

    while (i < 6) {
      n = getRandomInt(range.min, range.max);

      if ($.inArray(n, arrN) === -1) {
        arrN.push(n);
        i++;
      } else {
        continue;
      }
    }

    arrN.sort(function (a, b) {
      return a - b;
    });

    // Attributes the value to the input frame
    $.each(arrN, (_k, v) => {
      $(`#fr-elem${counter + 1}`).val(padLeft(v, 2));
      counter++;
    });

    // console.log(bInfo);
  });
});

"use strict";
$(function () {
    $(".frame-element").autotab({
        format: "number",
        maxlength: 2
    });
    $("#fr-elem1").focus();
    $("#btn-analisar-elem").on("click", () => {
        const serializedData = $Elements.serialize();
        if (DEV_ENV)
            console.log(serializedData);
        const data = {
            serializedData
        };
        $.ajax({
            url: `${ABS_HOME_URI}/modals/analysisResults.php`,
            type: "POST",
            data,
            success: (rs) => {
                const _rs = JSON.parse(rs);
                $("#loader-modal-content").fadeOut("fast");
                $("#body-modal-formAnalysis").addClass("text-left");
                $("#content-container").show("slow").html(_rs.responseText);
            },
            error: function (e) {
                console.log(e.responseText);
            }
        });
    });
    $DBCaching.init();
});
//# sourceMappingURL=form-analysis.js.map
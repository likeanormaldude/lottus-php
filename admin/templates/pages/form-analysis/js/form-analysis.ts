$(function () {
  $(".frame-element").autotab({
    format: "number",
    maxlength: 2
  });

  $("#fr-elem1").focus();

  $("#btn-analisar-elem").on("click", () => {
    const serializedData = $Elements.serialize();

    if (DEV_ENV) console.log(serializedData);

    const data: BodyRequestModal = {
      serializedData
    };

    $.ajax({
      url: `${ABS_HOME_URI}/modals/analysisResults.php`,
      dataType: "json",
      type: "POST",
      data,
      success: (rs) => {
        $("#loader-modal-content").fadeOut("fast");
        $("#body-modal-formAnalysis").addClass("text-left");
        $("#content-container").show("slow").html(rs.responseText);
      },
      error: function (e) {
        console.log(e.responseText);
      }
    });
  });

  /*$('#myModal').on('shown.bs.modal', function () {
    $('#myInput').trigger('focus')
  })*/

  $DBCaching.init();

  // $Elements.addClearBtnListeners(2, 6);

  // $("#icon-clear-elem1").click(() => {
  //   $(".frame-element").filter(":lt(6)").val("");
  //   $("#fr-elem1").focus();
  // });

  // $("#icon-clear-elem2").click(() => {
  //   $(".frame-element").filter(":gt(5)").val("");
  //   $("#fr-elem7").focus();
  // });
});

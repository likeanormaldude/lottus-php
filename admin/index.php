<?php

session_start();
require_once "../inc/constants.php";
require_once ABSPATH . '/inc/autoload.php';
require_once ABSPATH . '/vendor/autoload.php';
require_once ABSPATH . '/inc/functions.php';

checkSession();

if(DEV_ENV)
    setManualAppCachingConfig();

try {
    //Initializing objects
    $loader = new Twig\Loader\FilesystemLoader();

    includeTwigPaths([
        ABSPATH.'/admin/templates'
        ,ABSPATH.'/admin/templates/pages/home'
        ,ABSPATH.'/admin/templates/page-areas/header'
        ,ABSPATH.'/admin/templates/page-areas/sidebar'
        ,ABSPATH.'/admin/templates/page-areas/offset-options'
        ,ABSPATH.'/admin/templates/page-areas/footer'
        ,ABSPATH.'/admin/templates/sample-and-debug/srtdash-content-sample'
    ], $loader);

    $twig = new Twig\Environment($loader);
    $template = $twig->load('home.html.twig');

    //Context array to be assigned on Twig.
    $context = [
        'pathFromRoot' => 'admin/templates/pages/home/'
        ,'ABSPATH' => ABSPATH
        ,'ABS_HOME_URI' => ABS_HOME_URI
        ,'show_breadcrumbs' => 0
        ,'title' => 'Página Inicial'
        ,'subtitle' => 'Página Inicial'
    ];

    echo $template->render($context);
} catch (Exception $e) {
    die ('ERROR: '.$e->getMessage());
}

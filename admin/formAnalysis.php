<?php

session_start();
require_once "../inc/constants.php";
require_once ABSPATH . '/inc/autoload.php';
require_once ABSPATH . '/vendor/autoload.php';
require_once ABSPATH . '/inc/functions.php';

checkSession();

//require_once ABSPATH.'/staticData/static.php';
require_once ABSPATH.'/misc/Lottus/SessionManager.php';

try {
    //Initializing objects
    $loader = new Twig\Loader\FilesystemLoader();

    includeTwigPaths([
         ABSPATH.'/admin/templates'
        ,ABSPATH.'/admin/templates/pages/form-analysis'
        ,ABSPATH.'/admin/templates/page-areas/header'
        ,ABSPATH.'/admin/templates/page-areas/sidebar'
        ,ABSPATH.'/admin/templates/page-areas/offset-options'
        ,ABSPATH.'/admin/templates/page-areas/footer'
        ,ABSPATH.'/admin/templates/sample-and-debug/srtdash-content-sample'
    ], $loader);

    $twig = new Twig\Environment($loader);
    $template = $twig->load('form-analysis.html.twig');

    $basicGameInfo = SessionManager::getInstance()->getBasicGameInfo();

    $args = array_merge( $basicGameInfo, [
        'numberOfComponents' => 2,
        'args' => [
             [ 'componentTitle' => 'Primeira Sequência' ]
            ,[ 'componentTitle' => 'Segunda Sequência' ]
        ]
    ]);

    $inputFrameComponent = Elements::getComponent( Elements::COMP_INPUT_FRAMES, $args );
    $inputFrames = $inputFrameComponent
        ->marginVertical( 4 )
        ->makeComponent()
    ;

    $appCaching = SessionManager::getInstance()->getAppCachingConfig();

    //Context array to be assigned on Twig.
    $context = [
         'pathFromRoot' => 'admin/templates/pages/form-analysis/'
        ,'ABSPATH' => ABSPATH
        ,'ABS_HOME_URI' => ABS_HOME_URI
        ,'show_breadcrumbs' => 0
        ,'title' => 'Análise de Elementos'
        ,'subtitle' => 'Análise de Elementos'
        ,'formAnalysisInputFrames' => $inputFrames
        ,'precision' => $appCaching['precision']
    ];

    echo $template->render($context);
} catch (Exception $e) {
    die ('ERROR: '.$e->getMessage());
}

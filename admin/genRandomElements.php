<?php

session_start();
require_once "../inc/constants.php";
require_once ABSPATH . '/inc/autoload.php';
require_once ABSPATH . '/vendor/autoload.php';
require_once ABSPATH . '/inc/functions.php';

// Secondary Imports
require_once ABSPATH.'/misc/Lottus/SessionManager.php';

checkSession();

setManualAppCachingConfig();

try {
    //Initializing objects
    $loader = new Twig\Loader\FilesystemLoader();

    includeTwigPaths([
         ABSPATH.'/admin/templates'
        ,ABSPATH.'/admin/templates/pages/gen-random-elements'
        ,ABSPATH.'/admin/templates/page-areas/header'
        ,ABSPATH.'/admin/templates/page-areas/sidebar'
        ,ABSPATH.'/admin/templates/page-areas/offset-options'
        ,ABSPATH.'/admin/templates/page-areas/footer'
        ,ABSPATH.'/admin/templates/sample-and-debug/srtdash-content-sample'
    ], $loader);

    $twig = new Twig\Environment($loader);
    $template = $twig->load('gen-random-elements.html.twig');

    $SessionManager = SessionManager::getInstance();
    $occurrences = $SessionManager->getOccurrences( SessionManager::RETURN_ARRAY );
    $basicGameInfo = $SessionManager->getBasicGameInfo();
    $appCachingConfig = $SessionManager->getAppCachingConfig();

    // 6 most frequent elements
    $len = count( $occurrences );
    arsort($occurrences);
    $arrMostFrequent = array_slice( $occurrences, 0, 6 , TRUE);
    ksort($arrMostFrequent);

    $sequenceSet = array_keys( $arrMostFrequent );

    $sequenceDetails = getSequenceSetDetails( $sequenceSet, $arrMostFrequent );


    $args = array_merge( $basicGameInfo, [
         'numberOfComponents' => 1
        ,'args' => [
            [
                'occurrences' => $sequenceDetails['padded_str']['occurrences']
            ]
        ]
    ]);

    $inputFrameComponent = Elements::getComponent( Elements::COMP_INPUT_FRAMES, $args );

    $inputFramesMostFrequent = $inputFrameComponent
        ->disableInputs( TRUE )
//        ->coloredElements( FALSE )
        ->displayClearSequenceIcon( FALSE )
        ->marginVertical( 4 )
        ->assignInputFrameValuesByArray(
            [
                $sequenceDetails['padded_str']['elements']
            ])
        ->makeComponent()
    ;

    $componentTitle = "
        <span class='subtitle-small d-block mb-2 mt-4'>
            Sequência Gerada
        </span>
    ";

    $args = array_merge( $basicGameInfo, [
        'numberOfComponents' => 1
        ,'args' => [
            [
                'componentTitle' => new ComponentTitle( $componentTitle )
            ]
        ]
    ]);

    $inputFrames = $inputFrameComponent
        ->resetComponent()
        ->appendClassInputFrame( 'element-text-input-frame-generated' )
        ->setInputFrameCounter(6) // Countinues from the second comp, different settings.
        ->setArgs($args)
        ->assignInputFrameValuesByArray([])
        ->makeComponent()
    ;

    $debug=1;

    //Context array to be assigned on Twig.
    $context = [
         'pathFromRoot' => 'admin/templates/pages/gen-random-elements/'
        ,'ABSPATH' => ABSPATH
        ,'ABS_HOME_URI' => ABS_HOME_URI
        ,'show_breadcrumbs' => 0
        ,'title' => 'Análise de Elementos'
        ,'subtitle' => 'Análise de Elementos'
        ,'inputFramesMostFrequent' => $inputFramesMostFrequent
        ,'inputFrames' => $inputFrames
        ,'precision' => $appCachingConfig['precision']
    ];

    echo $template->render($context);
} catch (Exception $e) {
    die ('ERROR: '.$e->getMessage());
}

<?php

require_once "../inc/constants.php";
require_once ABSPATH.'/inc/autoload.php';
require_once ABSPATH.'/vendor/autoload.php';
require_once ABSPATH.'/inc/functions.php';

require_once ABSPATH.'/staticData/static.php';

try {
    //Initializing objects
    $loader = new Twig\Loader\FilesystemLoader();

    includeTwigPaths([
         ABSPATH.'/admin/templates'
        ,ABSPATH.'/admin/templates/pages/analysis-results'
        ,ABSPATH.'/admin/templates/page-areas/header'
        ,ABSPATH.'/admin/templates/page-areas/sidebar'
        ,ABSPATH.'/admin/templates/page-areas/offset-options'
        ,ABSPATH.'/admin/templates/page-areas/footer/'
        ,ABSPATH.'/admin/templates/sample-and-debug/srtdash-content-sample'
    ], $loader);

    $twig = new Twig\Environment($loader);
    $template = $twig->load('analysis-results.html.twig');



    //Context array to be assigned on Twig.
    $context = [
         'pathFromRoot' => 'admin/templates/pages/analysis-results'
        ,'ABSPATH' => ABSPATH
        ,'ABS_HOME_URI' => ABS_HOME_URI
        ,'show_breadcrumbs' => 0
        ,'title' => 'Estatística de Resultados'
        ,'subtitle' => 'Estatísticas de Resultados'
        ,'cardElements' => $cardElements
    ];

    echo $template->render($context);
} catch (Exception $e) {
    die ('ERROR: ' . $e->getMessage());
}

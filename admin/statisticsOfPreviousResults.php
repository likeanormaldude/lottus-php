<?php

session_start();
require_once "../inc/constants.php";
require_once ABSPATH . '/inc/autoload.php';
require_once ABSPATH . '/vendor/autoload.php';
require_once ABSPATH . '/inc/functions.php';

checkSession();


//require_once ABSPATH.'/staticData/static.php';
require_once ABSPATH.'/misc/Lottus/SessionManager.php';

try {
    //Initializing objects
    $loader = new Twig\Loader\FilesystemLoader();

//    setManualAppCachingConfig();

    includeTwigPaths([
         ABSPATH.'/admin/templates'
        ,ABSPATH.'/admin/templates/pages/statistics-of-previous-results'
        ,ABSPATH.'/admin/templates/page-areas/header'
        ,ABSPATH.'/admin/templates/page-areas/sidebar'
        ,ABSPATH.'/admin/templates/page-areas/offset-options'
        ,ABSPATH.'/admin/templates/page-areas/footer'
        ,ABSPATH.'/admin/templates/sample-and-debug/srtdash-content-sample'
    ], $loader);

    $twig = new Twig\Environment($loader);
    $template = $twig->load('statistics-of-previous-results.html.twig');

    $basicGameInfo = SessionManager::getInstance()->getBasicGameInfo();

    $args = array_merge( $basicGameInfo, []);

    $Elements = Elements::getComponent( Elements::COMP_CARD_ELEMENTS, $args );
    $Elements
        ->displayBottomLeft(TRUE)
        ->setBottomRightDisplayConfig( CardElementsComponent::DISPLAY_SCORE_AND_DATE )
    ;

    $SessionManager = SessionManager::getInstance();
    $appCaching = $SessionManager->getAppCachingConfig();
    $arrPrecision = $SessionManager->getPrecision();
    $occurrences = $SessionManager->getOccurrences( SessionManager::RETURN_PADDED_STR );

    // The most to the least recent contest
    rsort($arrPrecision['contests']);

    $cardsContests = "";

    if( count( $arrPrecision ) > 0 ){
        $argsComponent = [];
        $counter = 0;

        foreach( $arrPrecision['contests'] as $contest ){
            $sequenceDetails = getSequenceSetDetails( $contest['elements'], $occurrences );
            $dataSorteioFormatada = date('d/m/Y', strtotime( $contest['date'] ) );

            $argsComponent['args'][$counter] = [
                'subtitle' => '',
                'edition' => $contest['edition'],
                'occurrences' => $sequenceDetails['padded_str']['occurrences'],
                'elements' => $sequenceDetails['padded_str']['elements'],
                'score' => $sequenceDetails['padded_str']['score'],
                'dataSorteioFormatada' => $dataSorteioFormatada
            ];

            $counter++;
        }

        $cardsContests = $Elements->makeComponent($argsComponent);
    }

    //Context array to be assigned on Twig.
    $context = [
         'pathFromRoot' => 'admin/templates/pages/statistics-of-previous-results/'
        ,'ABSPATH' => ABSPATH
        ,'ABS_HOME_URI' => ABS_HOME_URI
        ,'show_breadcrumbs' => 0
        ,'title' => 'Estatística de Jogos Passados'
        ,'subtitle' => 'Estatística de Jogos Passados'
        ,'precision' => $appCaching['precision']
        ,'cardsContests' => $cardsContests
    ];

    echo $template->render($context);
} catch (Exception $e) {
    die ('ERROR: '.$e->getMessage());
}

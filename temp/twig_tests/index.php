<?php
// menu.php
// inclure  l'autoloader
require_once 'vendor/autoload.php';
require_once $_SERVER['CONTEXT_DOCUMENT_ROOT']."/inc/constants.php";
require_once ABSPATH."/inc/autoload.php";

try {
    // le dossier ou on trouve les templates
    $loader = new Twig\Loader\FilesystemLoader('.');

    // initialiser l'environement Twig
    $twig = new Twig\Environment($loader);

    // load template
    $template = $twig->load('test.html.twig');

    // set template variables
    // render template
    echo $template->render([
        'bool' => 0,
        'nome' => 'Deividson',
    ]);

    $Log = new Log();
    $Log->setOperacao('blabla');

} catch (Exception $e) {
    die ('ERROR: ' . $e->getMessage());
}

//echo 'blabl��';
<?php

require_once "inc/constants.php";
require_once ABSPATH.'/inc/autoload.php';
require_once ABSPATH .'/vendor/autoload.php';
require_once ABSPATH.'/inc/functions.php';

try{
    //Initializing objects
    $loader = new Twig\Loader\FilesystemLoader();

    includeTwigPaths([
         ABSPATH.'/templates/'
        ,ABSPATH.'/templates/pages/login'
    ], $loader);


    $twig       = new Twig\Environment($loader);
    $template   = $twig->load('login.html.twig');

    //Context array to be assigned on Twig.
    $context = [
         'ABSPATH' => ABSPATH
        ,'ABS_HOME_URI' => ABS_HOME_URI
        ,'show_breadcrumbs' => 0
        ,'title' => 'Login'
        ,'subtitle' => 'Login'
    ];

    echo $template->render($context);
}catch( Exception $e ){
    die ('ERROR: ' . $e->getMessage());
}
<?php

session_start();

// Dft Imports
require_once "../inc/constants.php";
require_once ABSPATH.'/inc/autoload.php';
require_once ABSPATH .'/vendor/autoload.php';
require_once ABSPATH.'/inc/functions.php';

$status = 500;
$responseText = '';

$Usuario = new Usuario();

$Usuario
    ->setUsuario( $_POST['usuario'] )
    ->setSenha( $_POST['senha'] );

$UsuarioModel = new UsuarioModel( $Usuario );
$rs = $UsuarioModel->select();

if( $rs === FALSE ){
    $responseText = APP_ERR_500;
}else if( $rs !== FALSE && count( $rs ) > 0 ){
    $status = 200;
    $responseText = "Login OK";
    $lastData = $UsuarioModel->getConn()->getLastFetchedData();
    $_SESSION['idusuario'] = (int) $lastData[0]['idusuario'];
}else{
    $responseText = "Usuário ou senha inválidos";
}

$encodedResponse = json_encode([
     'status' => $status
    ,'responseText' => $responseText
]);

echo $encodedResponse;

$debug=1;



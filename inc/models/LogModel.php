<?php

class LogModel extends MainModelAbstract implements iSingleTonModel {
    /**
     * @var LogModel $SingleTon
     * */
    private static $SingleTon;

    function __construct( Log $Log ){
        $this->Object  = $Log;
        $this->setConn(DB::getInstance());
    }

    /**
     * @param Log $Log
     */
    public static function getInstance($Log): LogModel{
        if(!self::$SingleTon)//Guarantee just one instance
            self::$SingleTon = new LogModel($Log);

        return self::$SingleTon;
    }


}


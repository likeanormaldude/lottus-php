<?php


class UsuarioModel extends MainModelAbstract implements iSingleTonModel {
    /**
     * @var UsuarioModel $SingleTon
     * */
    private static $SingleTon;

    function __construct( Usuario $Usuario ){
        $this->Object  = $Usuario;
        $this->setConn(DB::getInstance());
    }

    /**
     * @param Usuario $Usuario
     */
    public static function getInstance($Usuario){
        if(!self::$SingleTon)//Guarantee just one instance
            self::$SingleTon = new UsuarioModel($Usuario);

        return self::$SingleTon;
    }




}
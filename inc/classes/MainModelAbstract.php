<?php

abstract class MainModelAbstract{
    /**
     * @var MainClass
     */
    protected $Object;

    /**
     * @var DB
     */
    private $conn;

    /**
     * @return DB
     */
    public function getConn(){
        return $this->conn;
    }

    /**
     * @param DB $conn
     */
    protected function setConn(DB $conn): void{
        $this->conn = $conn;
    }

    public function insert($log = FALSE){
        return
            $this
                ->conn
                ->insert(
                    $this->Object->getTable()
                    ,$this->Object->getMatrixVal(),
                    $log
                )
            ;
    }

    public function delete( $where, $log = FALSE ){
        return $this->conn->delete($this->Object->getTable(), $where, $this->Object->getMatrixVal(), $log);
    }

    public function update( $where, $log = FALSE ){
        return $this->conn->update($this->Object->getTable(), $this->Object->getMatrixVal(), $where, [], $log);
    }

    /**
     * @param string|null $where Expects <b>AND</b> clause, since its
     * value is 'WHERE 1=1 '. <p>If NULL, the
     * method will try to consider the set attributes of the class
     * and test them on <b>WHERE</b> clause.</p><p> <i>i.e:
     * <br>&nbsp;&nbsp;WHERE 1=1 <br>
     * &nbsp;&nbsp;AND usuario = :usuario<br>
     * &nbsp;&nbsp;AND senha = :senha </i></p>
     * @return array|bool The result rows, if possible or <b>FALSE</b>
     * in case of any exception
     * */
    public function select( $where = NULL, $log = FALSE ){
        if( !!$where ){
            $w = " WHERE 1=1 ".$where;
        }else{
            $w = " WHERE 1=1 ".getWhereFieldsString( $this->Object->getMatrixVal() );
        }

        $querystring = "SELECT * FROM ".$this->Object->getTable().$w;
        return $this->conn->query( $querystring, $this->Object->getMatrixVal(), $log );
    }

    public function fetchAllRows( $where = NULL, $log = FALSE ){
        return $this->conn->query("SELECT * FROM ".$this->Object->getTable()." WHERE 1=1 ".$where, [], $log);
    }



}
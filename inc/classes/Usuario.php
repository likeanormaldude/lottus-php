<?php

class Usuario extends MainClass implements iSingleTon {

    const GRATIS    = 1;
    const PADRAO    = 2;
    const PREMIUM   = 3;


    /**
     * @var Usuario $SingleTon
     * */
    private static $SingleTon;

    /**
     * @var int $idusuario
     * */
    private $idusuario;

    /**
     * @var string $nome
     * */
    private $nome;

    /**
     * @var int $idade
     * */
    private $idade;

    /**
     * @var string $usuario
     * */
    private $usuario;

    /**
     * @var string $senha
     * */
    private $senha;

    public function __construct(){
        $this->table = 'usuario';
    }

    /**
     * @return int
     * @return self
     */
    public function getIdusuario(): int{
        return $this->idusuario;
    }

    /**
     * @param int $idusuario
     */
    public function setIdusuario(int $idusuario): self{
        $this->idusuario = $idusuario;
        $this->pushMatrixVal( (int) $idusuario);
        return $this;
    }

    /**
     * @return string
     * @return self
     */
    public function getNome(): string{
        return $this->nome;
    }

    /**
     * @param string $nome
     */
    public function setNome(string $nome): self{
        $this->nome = $nome;
        $this->pushMatrixVal($nome);
        return $this;

    }

    /**
     * @return int
     * @return self
     */
    public function getIdade(): int{
        return $this->idade;
    }

    /**
     * @param int $idade
     */
    public function setIdade(int $idade): self{
        $this->idade = $idade;
        $this->pushMatrixVal( (int) $idade);
        return $this;
    }

    /**
     * @return string
     */
    public function getUsuario(): string{
        return $this->usuario;
    }

    /**
     * @param string $usuario
     */
    public function setUsuario(string $usuario){
        $this->usuario = $usuario;
        $this->pushMatrixVal($usuario);
        return $this;
    }

    /**
     * @return string
     */
    public function getSenha(): string{
        return $this->senha;
    }

    /**
     * @param string $senha
     */
    public function setSenha(string $senha){
        $this->senha = $senha;
        $this->pushMatrixVal( $senha );
        return $this;
    }


    /**
     * @return self
     * */
    public static function getInstance(){
        //Guarantee just one instance
        if(!self::$SingleTon) self::$SingleTon = new Usuario();

        return self::$SingleTon;
    }


}
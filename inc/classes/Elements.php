<?php

/**
 * Designed to work as similar as possible to React component creator class.
 * */
class Elements{

    const COMP_CARD_ELEMENTS        = 'CARD_ELEMENTS';
    const COMP_INPUT_FRAMES         = 'INPUT_FRAMES';

    /**
     * @param string $ComponentName see Elements::COMP_* constants
     * @param array $args
     * @throws Exception
     * @return CardElementsComponent|InputFrameComponent|void The Element Component instance
     * */
    public static function getComponent( $ComponentName, $args = [] ){
        if( !$args || count( $args ) === 0 )
            throw new Exception('$args must be passed, none given');

        switch($ComponentName){
            case Elements::COMP_CARD_ELEMENTS:
                return CardElementsComponent::getInstance( $args );
            case Elements::COMP_INPUT_FRAMES:
                return InputFrameComponent::getInstance( $args );
        }

    }

    /**
     * @param array|int $mixedValue Can be either array or an integer. <p>
     * if it's an <b>Array</b>, the return will be the formatted array of occurrences.</p>
     * <p>In case it's an <b>Integer</b>, the return will be a string with the formatted
     * occurrence</p>
     * @return array|string
     * */
    public static function getFormattedOccurrenceArray( array|int $mixedValue ){
        if( is_array( $mixedValue ) ){
            $r = [];

            foreach($mixedValue as $v ){
                $r[] = '('.str_pad( $v, 2, '0', STR_PAD_LEFT ).'x)'; // (12x)
            }

            return $r;
        }else{
            return '('.str_pad( strval( $mixedValue ), 2, '0', STR_PAD_LEFT ).'x)';;
        }
    }

}
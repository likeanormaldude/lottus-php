<?php

use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;


/**
 * Mega Sena Cache Handler
 * @see https://console.firebase.google.com/project/mega-sena-818a2/overview
 * */
class MegaSenaCache implements iLottusCache {

    const GAME_PROFILE = 'megasena';

    /**
     * The local gameData written to <i>./cache/gameData/data.json</i>
     * @var array $gameData
     * */
    private $gameData;

    /**
     * @var array $gameConfig The game profile info associative array
     * */
    private $gameConfig;

    /**
     * @var string $baseURI
     * */
    private string $baseURI;

    /**
     * @var self $SingleTon
     * */
    private static $SingleTon;

    /**
     * @var GuzzleHttp\Client
     * */
    private Client $GuzzleClient;

    public function __construct(){
        $this->baseURI = MEGA_SENA_BASE_URI;
        $this->GuzzleClient = new Client();
        $this->gameConfig = [];
        $this->gameData = [];
    }

    public static function getInstance(): MegaSenaCache{
        //Guarantee just one instance
        if(!self::$SingleTon){
            self::$SingleTon = new MegaSenaCache();
        }

        return self::$SingleTon;
    }

    /**
     * Gets the cached game data from <i>'./cache/gameData/data.json'</i>
     * @return array
     */
    public function getGameData(): array{
        if( count( $this->gameData ) === 0 ){
            $c = file_get_contents( GAME_DATA_DIR.'/data.json' );
            $this->gameData = json_decode( $c, TRUE );
        }

        return $this->gameData;
    }

    public function feedCache( &$exc_msg = NULL ): bool{
        try {
            $response = $this->GuzzleClient->request('GET', $this->baseURI.'/contests/all');
        } catch ( RequestException $e) {
            $exc_msg = $e->getResponse()->getBody()->getContents();
            return FALSE;
        }

        $body       = $response->getBody();

        $rs         = json_decode( $body, TRUE );

        $data       = [
            'gameProfile' => self::GAME_PROFILE
            ,'contests' => $rs
        ];

        $this->gameData = $data;

        $encoded    = json_encode( $data, JSON_PRETTY_PRINT );
        $filepath   = GAME_DATA_DIR.'/data.json';

        $f          = fopen($filepath, 'w');
        $write      = fwrite($f, $encoded);

        fclose( $f );

        if( $write === FALSE ){
            $exc_msg = "Couldn't write ".$filepath;
            return FALSE;
        }

        return TRUE;
    }

    /**
     * Gets the game basic info such as 'numberOfElements', 'range' and etc.
     * @return array The associative array with the data
     * */
    public function getBasicGameInfo(){

        if( count( $this->gameConfig ) > 0 ) return $this->gameConfig;

        $c = file_get_contents( GAME_CONFIG_DIR.'/megasena.json' );
        $gameConfig = json_decode( $c, TRUE );

        // Valid associative array ( != NULL )
        if( isset( $gameConfig ) ){
            $this->gameConfig = $gameConfig;
        }else
            throw new Exception( EXC_CACHE_MEGASENA_CONFIG_INVALID );

        return $gameConfig;
    }

}
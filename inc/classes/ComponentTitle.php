<?php

class ComponentTitle{

    private string $title;

    public function __construct( string $t ){
        $this->title = $t;
    }

    public function getTitle(){
        return $this->title;
    }

}
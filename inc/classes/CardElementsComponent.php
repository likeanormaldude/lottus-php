<?php

/**
 * Card Elements Component Class
 * */
class CardElementsComponent{

    const ELEMENT_CELL              = 1;
    const OCCURENCE_CELL            = 2;
    const DISPLAY_SCORE             = 1;
    const DISPLAY_DATE              = 2;
    const DISPLAY_SCORE_AND_DATE    = 3;

    // Top
    private bool $displayTopLeft        = FALSE;
    private bool $displayTopCenter      = TRUE;
    private bool $displayTopRight       = TRUE;

    // Bottom
    private bool $displayBottomLeft     = FALSE;
    private bool $displayBottomCenter   = FALSE;
    private bool $displayBottomRight    = TRUE;

    /**
     * @var int $componentCounter Increases every time the row of
     * the component changes. <p> The row is a new
     * subset of all small HTML components that are part of
     * the layout configuration of this object.
     * */
    private $componentCounter = 0;

    private string $outputHTML = '';


    /**
     * @var array $args An associative array to be assigned on the respective component
     * */
    private array $args;

    /**
     * @var CardElementsComponent $SingleTon
     * */
    private static $SingleTon;

    /**
     * @var int $bottomRightDisplayConfig <p>Display the <i>score</i> (1) of the contest,
     * <i>only the date</i> (2) or <i>both</i> (3).
     * */
    private $bottomRightDisplayConfig = self::DISPLAY_SCORE;

    public function __construct( array $args = [] ){
        $this->args = $args;
    }

    public function makeComponent($args = []){
        $this->args = array_merge( $this->args, $args);
        $this->_makeCardElements();
        return $this->outputHTML;
    }

    /**
     * @return CardElementsComponent
     */
    public static function getInstance( array $args = [] ){
        if(!self::$SingleTon)//Guarantee just one instance
            self::$SingleTon = new CardElementsComponent( $args );

        return self::$SingleTon;
    }

    /**
     * @param string $html
     * @param array $args
     * @return void Only modifies the html variable
     * */
    private function _makeCardElements(){

        $delimiter = array_key_exists('numberOfComponents', $this->args) ?
            $this->args['numberOfComponents'] :
            count($this->args['args']);

        for( $i=0; $i < $delimiter; $i++ ){

            if( isset( $this->args['args'][$this->componentCounter]['componentTitle'] ) ){
                $componentTitle = $this->args['args'][$this->componentCounter]['componentTitle'];
            }else{
                $componentTitle = '';
            }

            if( isset( $this->args['args'][$i]['occurrences'] ) ){
                $paramOcc = $this->args['args'][$i]['occurrences'];
            }else{
                $paramOcc = NULL;
            }

            if( isset( $this->args['args'][$i]['elements'] ) ){
                $paramElems = $this->args['args'][$i]['elements'];
            }else{
                $paramElems = NULL;
            }

            $topLeftHTML = $this->_getTopLeftCardHTML();
            $topCenterHTML = $this->_getTopCenterCardHTML();
            $topRightHTML = $this->_getTopRightCardHTML();

            $bottomLeftHTML = $this->_getBottomLeftCardHTML();
            $bottomCenterHTML = $this->_getBottomCenterCardHTML();
            $bottomRightHTML = $this->_getBottomRightCardHTML();

            $htmlOccurrencesCells = $this->_getCardCells( $paramOcc, self::OCCURENCE_CELL );
            $htmlElementsCells = $this->_getCardCells( $paramElems, self::ELEMENT_CELL );

            $this->outputHTML .= "
              <div class='card-body'>
                <div class='d-flex justify-content-center align-items-center mb-0'>
                  <h5 class='mb-1 subtitle-elements'>".$componentTitle."</h5>
                </div>
                <div class='row justify-content-center'>
                  <div class='col col-md-8'>
                    <div class='card-result'>
                      <div class='card-elements-area'>
                        <div
                          class='row justify-content-md-center justify-content-lg-center'
                        >
                          <div class='col col-sm-12 col-md-12 d-flex justify-content-center'>
                            <div id='temp' class='table-responsive'>
                              <table class='table no-margin-bottom container-card-elements'>
                                <thead>
                                  <tr class='card-elements-header'>
                                    <th
                                      scope='col'
                                      style='width: 33%'
                                      class='text-left'
                                    >
                                      ".$topLeftHTML."
                                    </th>
                                    <th
                                      scope='col'
                                      style='width: 33%'
                                      colspan='1'
                                      class='text-center title-elements'
                                    >
                                      ".$topCenterHTML."
                                    </th>
                                    <th
                                      scope='col'
                                      style='width: 33%'
                                      class='text-right'
                                    >
                                      ".$topRightHTML."
                                    </th>
                                  </tr>
                                </thead>
                                <tbody>
                                  <tr>
                                    <td colspan='3'>
                                      <div class='table-responsive'>
                                        <table class='table no-margin-bottom table-elements'>
                                          <tbody>
                                            <tr>
                                              ".$htmlOccurrencesCells."
                                            </tr>
                                            <tr id='row-elements".$this->componentCounter."'>
                                            ".$htmlElementsCells."
                                            </tr>
                                          </tbody>
                                        </table>
                                      </div>
                                    </td>
                                  </tr>
                                  <tr>
                                    <td class='text-left'>".$bottomLeftHTML."</td>
                                    <td class='text-center'>".$bottomCenterHTML."</td>
                                    <td class='text-right'>".$bottomRightHTML."</td>
                                  </tr>
                                </tbody>
                              </table>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            ";

            $this->componentCounter++;
        }

    }

    private function _getTopLeftCardHTML(){
        if(!$this->displayTopLeft) return '';
        return 'Top Left';
    }

    private function _getTopCenterCardHTML(){
        if(!$this->displayTopCenter) return '';

        if( isset($this->args['args'][$this->componentCounter]['edition']) ){
            return "Concurso de nº ".$this->args['args'][$this->componentCounter]['edition'];
        }else{
            return "Sorteio ".strval( ( $this->componentCounter+1 ) );
        }
    }


    private function _getTopRightCardHTML(){
        if(!$this->displayTopRight) return '';

        return '<i class="imoon-bar-chart card-element-icon"></i>';
    }

    private function _getBottomLeftCardHTML(){
        if(!$this->displayBottomLeft) return '';

        // Gets the counter without increasing it
        return '
            <i 
                onclick="$Elements.copyElementsToClipboard('.$this->componentCounter.')" 
                class="imoon-content_copy card-element-icon">
            </i>';
    }

    private function _getBottomCenterCardHTML(){
        if(!$this->displayBottomCenter) return '';
        return 'Bottom Center';
    }

    private function _getBottomRightCardHTML(){
        if(!$this->displayBottomRight) return '';

        switch($this->bottomRightDisplayConfig){
            case self::DISPLAY_SCORE:
                return 'Score total: '.$this->args['args'][$this->componentCounter]['score'].' pts';
            case self::DISPLAY_DATE:
                return $this->args['args'][$this->componentCounter]['dataSorteioFormatada'];
            case self::DISPLAY_SCORE_AND_DATE:
                return $this->args['args'][$this->componentCounter]['dataSorteioFormatada'].' | 
                '.'Score total: '.$this->args['args'][$this->componentCounter]['score'].' pts';
            default:
                return '';
        }
    }

    /**
     * Whether should display bottom left or not
     * */
    public function displayBottomLeft( bool $dbl ){
        $this->displayBottomLeft = $dbl;
        return $this;
    }

    /**
     * Whether should display bottom left or not
     * */
    public function displayBottomCenter( bool $dbc ){
        $this->displayBottomCenter = $dbc;
        return $this;
    }

    /**
     * Whether should display bottom right or not
     * */
    public function displayBottomRight( bool $dbr ){
        $this->displayBottomRight = $dbr;
        return $this;
    }

    /**
     * Whether should display top left or not
     * */
    public function displayTopLeft( bool $dtl ){
        $this->displayTopLeft = $dtl;
        return $this;
    }

    /**
     * Whether should display top center or not
     * */
    public function displayTopCenter( bool $dtc ){
        $this->displayTopCenter = $dtc;
        return $this;
    }

    /**
     * Whether should display top right or not
     * */
    public function displayTopRight( bool $dtr ){
        $this->displayTopRight = $dtr;
        return $this;
    }

    private function _getCardCells( array|NULL $collection, int $cellType ){
        $_html = '';

        if( $collection ) {
            //        $_counter = 0;
            foreach( $collection as $v ){
                $textClass = ( ( $cellType === self::ELEMENT_CELL ) ? 'element-text' : 'occurrence-text' );
//            $bg = ( ( $_counter %2 === 0 ) ? ' bg1 ' : ' bg2 ' );
                $_html .= "<td class='text-center ".$textClass."'>".$v."</td>";
//            $_counter++;
            }
        }else{
            for( $i=0; $i < $this->args['numberOfElements']; $i++ ){
                $_html .= "<td class='text-center'></td>";
            }
        }

        return $_html;
    }

    public function setBottomRightDisplayConfig( int $brdc ){
        $this->bottomRightDisplayConfig = $brdc;
    }

    /**
     * @param array $args
     */
    public function setArgs(array $args): void
    {
        $this->args = $args;
    }



}
<?php

/**
 * Generates the correct breadcrumb path according to SRTDash Template
 * */
class BreadCrumbsSRTDash{

    /**
     * @var array $breadcrumbs
     * @example Must be an regular array where the last position is the current page.
     * */
    private $breadcrumbs;

    public function __construct( array $breadcrumbs = [] ){
        $this->breadcrumbs = $breadcrumbs;
    }

    /**
     * @param array $breadcrumbs
     * @throws Exception
     * @return $this
     */
    public function setBreadcrumbs(array $breadcrumbs){
        if(count($breadcrumbs) === 0)
            throw new Exception('\$breadcrumbs should not be an empty array');

        $this->breadcrumbs = $breadcrumbs;

        return $this;
    }

    /**
     * @throws Exception
     * @return string Generated Breadcrumb html path
     */
    public function generateBreadcrumbs(){
        if(count($this->breadcrumbs) === 0)
            throw new Exception('\$breadcrumbs should not be an empty array. Use the setBreadcrumbs');

        $html = '<ul class="breadcrumbs pull-left">';

        foreach( $this->breadcrumbs as $k => $page ){
            $html .= '<li>';

            if( !!get_next_key_array( $this->breadcrumbs, $k ) ){
//                $html .= '<a href="assets/srtdash/index.html">Home</a>':
            }

        }
    }



}
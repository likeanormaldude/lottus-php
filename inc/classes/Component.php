<?php

/**
 * Designed to work as similar as possible to React component creator class.
 * */
class Component{

    /**
     * @var array $args An associative array to be assigned on the respective component
     * */
    private $args;

    const COMP_CARD_ELEMENTS = 'CARD_ELEMENTS';
    const COMP_WHATEVER = 'WHATEVER';

    private $counter = 0;

    public function __construct( array $args = [] ){
        $this->args = $args;
    }

    private function _getCounter(){
        $this->counter++;
        return str_pad( strval( $this->counter ), 2, "0" , STR_PAD_LEFT);
    }


    /**
     * @param string $compName see COMP_* constants
     * @param array $args
     * @throws Exception
     * @return string|void The component HTML code
     * */
    public function getComponent( $compName, $args = [] ){
        $_args = ( ( count( $args ) > 0 ) ? $args : $this->args );
        $html = '';
        $counter = 1;

        if( !$_args || count( $_args ) === 0 )
            throw new Exception('$args must be passed, none given');

        switch($compName){
            case Component::COMP_CARD_ELEMENTS:
                $html = "
                  <div class='card-body'>
                    <div class='d-flex justify-content-center align-items-center mb-0'>
                      <span class='text-muted'>
                        Dados com base em um total de 150 sorteios
                      </span>
                    </div>
                    <div class='row justify-content-center'>
                      <div class='col col-md-8'>
                        <div class='card-result'>
                          <div class='card-elements-area'>
                            <div
                              class='row justify-content-md-center justify-content-lg-center'
                            >
                              <div class='col col-sm-12 col-md-12 bla'>
                                <div class='table-responsive'>
                                  <table class='table no-margin-bottom'>
                                    <thead>
                                      <tr class='card-elements-header'>
                                        <th
                                          scope='col'
                                          style='width: 33%'
                                          class='col-4 text-left'
                                        >
                                          XX
                                        </th>
                                        <th
                                          scope='col'
                                          style='width: 33%'
                                          colspan='1'
                                          class='text-center col-4'
                                        >
                                          Sorteio ".$this->_getCounter()."
                                        </th>
                                        <th
                                          scope='col'
                                          style='width: 33%'
                                          class='col-4 text-right'
                                        >
                                          XX
                                        </th>
                                      </tr>
                                    </thead>
                                    <tbody>
                                      <tr>
                                        <td colspan='3'>
                                          <div class='table-responsive'>
                                            <table class='table no-margin-bottom'>
                                              <tbody>
                                                <tr>
                                                  <td class='text-center'>(14x)</td>
                                                  <td class='text-center'>(16x)</td>
                                                  <td class='text-center'>(12x)</td>
                                                  <td class='text-center'>(28x)</td>
                                                  <td class='text-center'>(30x)</td>
                                                  <td class='text-center'>(9x)</td>
                                                  <td class='text-center'>(35x)</td>
                                                  <td class='text-center'>(21x)</td>
                                                  <td class='text-center'>(14x)</td>
                                                  <td class='text-center'>(23x)</td>
                                                  <td class='text-center'>(6x)</td>
                                                  <td class='text-center'>(8x)</td>
                                                </tr>
                                                <tr>
                                                  <td class='text-center'>06</td>
                                                  <td class='text-center'>09</td>
                                                  <td class='text-center'>10</td>
                                                  <td class='text-center'>13</td>
                                                  <td class='text-center'>18</td>
                                                  <td class='text-center'>20</td>
                                                  <td class='text-center'>23</td>
                                                  <td class='text-center'>28</td>
                                                  <td class='text-center'>29</td>
                                                  <td class='text-center'>36</td>
                                                  <td class='text-center'>40</td>
                                                  <td class='text-center'>54</td>
                                                </tr>
                                              </tbody>
                                            </table>
                                          </div>
                                        </td>
                                      </tr>
                                      <tr>
                                        <td class='text-left'>AA</td>
                                        <td class='text-center'>AA</td>
                                        <td class='text-right'>Score total: 296 pts</td>
                                      </tr>
                                    </tbody>
                                  </table>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                ";
                break;
        }

        return $html;

    }

}
<?php
require_once ABSPATH.'/vendor/autoload.php';
require_once ABSPATH.'/misc/GetApacheAndPHPLogs/GetApacheAndPHPLogs.php';

use TRegx\CleanRegex\Pattern;
use Twig\TemplateWrapper;
use TRegx\CleanRegex\Match\Details\Detail;

class DebugHTML{

    const SIMPLE_DEBUG_CONTENT  = 1;
    const DB_DEBUG_CONTENT      = 2;

    private static $SingleTon;
    private $content;

    /**
     * @var bool $displayOnScreen
     * */
    private $displayOnScreen;

    /**
     * @var bool $redirectTo500
     * */
    private $redirectTo500;

    /**
     * @param string $content
     * */
    public function __construct( $content = NULL ){
        $this->content = $content;

        // Get the value set on DEV_ENV. In development environment, should always output debug;
        $this->displayOnScreen = DEV_ENV;
        $this->redirectTo500 = !DEV_ENV;
    }

    /**
     * @param string|null $content
     */
    public function setContent($content){
        $this->content = $content;
        return $this;
    }

    /**
     * @param bool $displayOnScreen Weather the output HTML should be displayed on screen or writen in the
     * Debug HTML physical file
     * */
    public function displayOnScreen( bool $displayOnScreen ){
        $this->displayOnScreen = $displayOnScreen;
        return $this;
    }

    /**
     * @param bool $redirect Weather the class should redirect to <i>500.php</i>
     * or not
     * */
    public function redirectTo500( bool $redirect ){
        $this->redirectTo500 = $redirect;
        return $this;
    }

    /**
     * @param \Twig\TemplateWrapper $template The output template
     * @param array $contextParams The associative array the will be sent to the template
     * in order to bind its variables
     * @return void
     * */
    private function _debug( TemplateWrapper &$template, array $contextParams ){
        $output = $template->render( $contextParams );

        $file = fopen(DEBUG_FILE_PATH, 'w');
        fwrite($file, $output);
        fclose($file);

        if( $this->displayOnScreen ){
            echo $output;
        }else if( $this->redirectTo500 ){
            header('location:'.ABS_HOME_URI.'/500.php');
        }
    }

    /**
     * Checks if the given content is Google 404 response error
     * @param string $content
     * @return string The content without tags or in case it's Google 404, the custom template
     * */
    private function _checkGoogle404( &$content = NULL ){
        $pattern404 = "(<a[\s]href=.+?www.google.com.+?<\/a>)([\s\S]+<\/ins>)";
        // Check if the contents is google 404 error
        $p404Google = Pattern::of( $pattern404 );

        $content = str_replace('’', '\'', $this->content);

        if( $p404Google->test( $content ) ){
            $p404Google->match( $content )->forEach( function ( Detail $detail) use( &$content ) {
                $_content = $detail->group(2)->text();
                $google404Content = rip_tags( $_content );
                $content = "<h3>Google 404 Error</h3>".$google404Content;
            });
        }else{
            $content = rip_tags( $content );
        }

        return $content;
    }

    /**
     * @param int $type Can be 1 DebugHTML::SIMPLE_DEBUG_CONTENT or 2 DebugHTML::DB_DEBUG_CONTENT
     * @param null|DB $dbInstance
     * @return void|bool
     * */
    public function debug($type = DebugHTML::SIMPLE_DEBUG_CONTENT, $dbInstance = NULL ){
        if(!$this->content) return FALSE;

        try{
            //Initializing objects
            $loader     = new Twig\Loader\FilesystemLoader(ABSPATH.'/templates/sample-and-debug/debugHTML/');
            $twig       = new Twig\Environment($loader);

            $this->_checkGoogle404( $content );

            $template   = $twig->load('debug.html.twig');

            //Context array to be assigned on Twig.
            $context = [
                 'renderType'           => $type
                ,'content'              => $content
                ,'ABS_HOME_URI'         => ABS_HOME_URI
                ,'pathFromRoot'         => 'templates/sample-and-debug/debugHTML'
            ];

            if(!!$dbInstance){
                $context = array_merge( $context, [
                     'lastTransactionType'  => $dbInstance->getLastTransactionType()
                    ,'lastPreparedStmt'     => $dbInstance->getLastPreparedStmtQuery()
                    ,'lastExecutedQuery'    => $dbInstance->getLastExecutedQuery()
                ]);
            }

            // PHP Error Log
            $GetApacheAndPHPLogs = new GetApacheAndPHPLogs();
            $context['phpErrorLog'] = $GetApacheAndPHPLogs->getPHPErrorLog(1);
            $this->_debug( $template, $context );
        }catch( Exception $e ){
            die ('ERROR: ' . $e->getMessage());
        }
    }

    /**
     * @return self
     * */
    public static function getInstance( $content = NULL ){
        if(!self::$SingleTon)//Guarantee just one instance
            self::$SingleTon = new DebugHTML( $content );

        return self::$SingleTon;
    }
}
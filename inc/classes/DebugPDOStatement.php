<?php
class DebugPDOStatement extends \PDOStatement{
    private $bound_variables=array();
    protected $pdo;

    protected function __construct($pdo) {
        $this->pdo = $pdo;
    }

    public function bindValue($parameter, $value, $data_type=\PDO::PARAM_STR){
        $this->bound_variables[$parameter] = (object) array('type'=>$data_type, 'value'=>$value);
        return parent::bindValue($parameter, $value, $data_type);
    }

    public function bindParam($parameter, &$variable, $data_type=\PDO::PARAM_STR, $length=NULL , $driver_options=NULL){
        $this->bound_variables[$parameter] = (object) array('type'=>$data_type, 'value'=>&$variable);
        return parent::bindParam($parameter, $variable, $data_type, $length, $driver_options);
    }

    public function debugBindedVariables(){
        $vars=array();

        foreach($this->bound_variables as $key=>$val){
            $vars[$key] = $val->value;

            if($vars[$key]===NULL)
                continue;

            switch($val->type){
                case \PDO::PARAM_STR: $type = 'string'; break;
                case \PDO::PARAM_BOOL: $type = 'boolean'; break;
                case \PDO::PARAM_INT: $type = 'integer'; break;
                case \PDO::PARAM_NULL: $type = 'null'; break;
                default: $type = FALSE;
            }

            if($type !== FALSE)
                settype($vars[$key], $type);
        }

        if(is_numeric(key($vars)))
            ksort($vars);

        return $vars;
    }

    public function debugQuery(){
        $queryString            = $this->queryString;
        $vars                   = $this->debugBindedVariables();
        $params_are_numeric     = is_numeric(key($vars));
        $arrWithPlaceholders    = [];

        foreach($vars as $key=>&$var){

            switch(gettype($var)){
                case 'string' :
                    $var = "'{$var}'";
                break;
                case 'integer' :
                    $var = "{$var}";
                break;
                case 'boolean' :
                    $var = $var ? 'TRUE' : 'FALSE';
                break;
                case 'NULL':
                    $var = 'NULL';
            }

            $arrWithPlaceholders[':'.$key] = $var;
        }

        if($params_are_numeric){
            $queryString = preg_replace_callback( '/\?/', function($match) use( &$arrWithPlaceholders) { return array_shift($arrWithPlaceholders); }, $queryString);
        }else{
            $queryString = strtr($queryString, $arrWithPlaceholders);
        }

        return $queryString.PHP_EOL;
    }
}
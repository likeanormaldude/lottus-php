<?php
class DebugPDO extends \PDO{
    public function __construct($dsn, $username = "", $password = "", $driver_options = array()){
        $driver_options[\PDO::ATTR_STATEMENT_CLASS] = array('DebugPDOStatement', array($this));
        $driver_options[\PDO::ATTR_PERSISTENT] = FALSE;
        parent::__construct($dsn, $username, $password, $driver_options);
    }
}
<?php
require 'vendor/autoload.php';
require_once 'inc/constants.php';
use Kreait\Firebase\Factory;
use Kreait\Firebase\ServiceAccount;

/**
 * Connection Firebase class
 * */
class MegaSenaRobotFirebase{
    /**
     * @var Kreait\Firebase\Database $database
     * */
    private $database;

    /**
     * @var Kreait\Firebase\Factory $factory
     * */
    private $factory;

    static private $instance;

    public function __construct( $jsonSettings = KEY_FILE_MEGA_SENA_ROBOT_FILEPATH ){
        $this->connect($jsonSettings);
    }

    public function connect( string $jsonSettings ){
        $serviceAccount = ServiceAccount::fromValue($jsonSettings);
        $this->factory = (new Factory)->withServiceAccount($serviceAccount);
        $this->database = $this->factory->createDatabase();
    }

    public function experimental_connect($jsonSettings){
        $serviceAccount = ServiceAccount::fromValue($jsonSettings);
        $this->factory = (new Factory)->withServiceAccount($serviceAccount);
        $this->database = $this->factory->createDatabase();
    }

    /**
     * @return Factory
     */
    public function getFactory(): Factory{
        return $this->factory;
    }

    /**
     * @return $this
     * */
    static public function getInstance(){
        if( !self::$instance ){
            self::$instance = new Conn();
        }

        return self::$instance;
    }

    /**
     * @return Kreait\Firebase\Database
     */
    public function getDatabase(){
        return $this->database;
    }

    /**
     * @param string $ref
     * @param string $child
     * */
    public function fetch( $child = '/' ){
        $ret = NULL;
        try{
            if( $rs = $this->database->getReference()->getSnapshot()->getChild($child) ){
                $ret = $rs->getValue();
            }
        }catch( Exception $e ){
            $ret = $e->getMessage();
        }

        return $ret;
    }

    /**
     * This method also stands for update with the given $refInsertingNode
     *
     * */
    public function insert( array $data, $refInsertingNode = '/' ){
        try{
            foreach( $data as $k => $v ){
                $this
                    ->database
                    ->getReference()
                    ->getChild($refInsertingNode)
                    ->getChild($k)
                    ->set($v)
                ;
            }
        }catch( Exception $e ){
            echo $e->getMessage();
        }

        return $this;
    }

    /**
     * @param string $child
     * */
    public function delete($child){
        if( $this->database->getReference()->getSnapshot()->hasChild($child) ){
            $this->database->getReference()->getChild($child)->remove();
            return $this;
        }else{
            return FALSE;
        }
    }

}
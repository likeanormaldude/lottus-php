<?php

/**
 * A class designed to produce the correct cache object
 * */
class LottusCacheFactory implements iLottusCache {

    const OBJ_MEGA_SENA = 'MegaSenaCache';
    const OBJ_JOGO_BIXO = 'JogoBixoCache';

    /**
     * Responsible for the object <i>'appCaching.json'</i>
     * @see https://gitlab.com/likeanormaldude/lottus-php/-/tree/dev/tmp
     * @var array $appCaching
     * */
    private $appCaching;

    /**
     * The local gameData written to <i>./cache/gameData/data.json</i>
     * */
    private $gameData;

    /**
     * @var self $SingleTon
     * */
    private static $SingleTon;

    /**
     * @var MegaSenaCache $CacheObjInstance
     * */
    private $CacheObjInstance;

    /**
     * @var array $mGameProNameToCacheInstance
     * */
    private $mGameProNameToCacheInstance;

    /**
     * @param string $ObjectName see OBJ_*
     * */
    public function __construct( $ObjectName = NULL ){

        $this->mGameProNameToCacheInstance = [
            'megasena' => LottusCacheFactory::OBJ_MEGA_SENA
        ];

        //Stores the Cache Object Instance to private attr $CacheObjInstance
        $this->gameData = [];
        $this->_assignAppCaching();
        $this->_assignCacheObjInstance( $ObjectName );
    }

    /**
     * Gets the cached game data from <i>'./cache/gameData/data.json'</i>
     * @return array
     */
    public function getGameData(){
        return $this->CacheObjInstance->getGameData();
    }

    /**
     * Get what's stored on <i>'appCaching.json'</i>
     * @see https://gitlab.com/likeanormaldude/lottus-php/-/tree/dev/tmp
     * @throws Exception In case of <i>'appCaching.json'</i> is invalid or doesn't exist
     * @return void
     * */
    private function _assignAppCaching(){
        // Check this modification. Just in theory, for now.
        $this->appCaching = SessionManager::getInstance()->getAppCachingConfig();
    }

    /**
     * @param string $ObjectName see OBJ_*
     * @throws Exception
     * @return void
     */
    private function _assignCacheObjInstance( $ObjectName = NULL ){
        if( !isset( $ObjectName ) ) {
            $gameProfile = $this->appCaching["gameProfile"];
            $oName = $this->_getCacheObjInstanceNameByGameProfile( $gameProfile );
        }else{
            $oName = $ObjectName;
        }

        switch( $oName ){
            case LottusCacheFactory::OBJ_MEGA_SENA:
                $this->CacheObjInstance = MegaSenaCache::getInstance();
                break;
            default:
                throw new Exception( '
                    "'.$ObjectName.'" is not valid as a Lottus Cache Object name
                ' );
        }

    }

    /**
     * @param string $ObjectName see OBJ_*
     * @return self
     */
    public static function getInstance( $ObjectName = NULL ){
        //Guarantee just one instance
        if(!self::$SingleTon){
            // Self Instance
            self::$SingleTon = new LottusCacheFactory( $ObjectName );
        }

        return self::$SingleTon;
    }

    /**
     * Returns only the Cache Instance name based on game profile
     * @param string $gameProfileNme i.e 'megasena'
     * @return string
     * */
    private function _getCacheObjInstanceNameByGameProfile( string $gameProfileNme ){
        return $this->mGameProNameToCacheInstance[$gameProfileNme];
    }

    public function feedCache( &$exc_msg = NULL ): bool{
        $cacheOK = FALSE;

        if( !checkCacheStructure( $exc_msg_structure ) ){
            $exc_msg = $exc_msg_structure;
            return FALSE;
        }

        $feedCache = $this->CacheObjInstance->feedCache( $exc_msg_feed_cache );

        if ($feedCache)
            $cacheOK = TRUE;
        else
            $exc_msg = $exc_msg_feed_cache;

        return $cacheOK;
    }

}


<?php

class Log extends MainClass implements iSingleTon{

    /**
     * @var self $SingleTon
     * */
    private static $SingleTon;

    private
         $idusuario
        ,$operacao
        ,$conteudo
        ,$dataTransacao
        ,$browser
    ;

    function __construct(){
        $this->table = 'log';
    }

    public static function getInstance(){
        //Guarantee just one instance
        if(!self::$SingleTon) self::$SingleTon = new Log();

        return self::$SingleTon;
    }

    /**
     * @return mixed
     */
    public function getIdusuario(){ return $this->idusuario; }

    /**
     * @param int $idusuario
     */
    public function setIdusuario($idusuario){
        $this->idusuario = $idusuario;
        $this->pushMatrixVal($idusuario);
        return $this;
    }

    /**
     * @return mixed
     */
    public function getOperacao(){ return $this->operacao; }

    /**
     * @param mixed $operacao
     */
    public function setOperacao($operacao){
        $this->operacao = $operacao;
        $this->pushMatrixVal($operacao);
        return $this;
    }

    /**
     * @return mixed
     */
    public function getConteudo(){ return $this->conteudo; }

    /**
     * @param string $conteudo
     */
    public function setConteudo($conteudo){
        $this->conteudo = $conteudo;
        $this->pushMatrixVal($conteudo);
        return $this;
    }

    /**
     * @return mixed
     */
    public function getDataTransacao(){ return $this->dataTransacao; }

    /**
     * @param string $dataTransacao
     */
    public function setDataTransacao($dataTransacao){
        $this->dataTransacao = $dataTransacao;
        $this->pushMatrixVal($dataTransacao);
        return $this;
    }

    /**
     * @return mixed
     */
    public function getBrowser(){ return $this->browser; }

    /**
     * @param string $browser
     */
    public function setBrowser( $browser ){
        $this->browser = $browser;
        $this->pushMatrixVal($browser);
        return $this;
    }

}
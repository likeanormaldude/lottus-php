<?php

require_once ABSPATH . '/inc/dao/auxFunctions.php';
require_once ABSPATH . '/inc/functions.php';

class MainClass{
    private $matrixVal = [];

    /**
     * @var string $table
     * */
    protected $table;

    /**
     * @return string
     */
    public function getTable(): string{ return $this->table; }

    public function getMatrixVal(){ return $this->matrixVal; }

    /**
     * In cases that some vars are manipulated/set and the following action is an update, for instance.
     * In this moment, unsetMatrixVal comes to reset the array with '[]'
     * @return $this
     * */
    public function unsetMatrixVal(){
        $this->matrixVal = [];
        return $this;
    }

    /**
     * This method must be called after each and every set on classes.
     * @param mixed $v
     * */
    protected function pushMatrixVal( $v ){
        if( count( $this->matrixVal ) === 0 ) $this->matrixVal = [];

        $callingMethod = debug_backtrace()[1]['function'];
        $f = lcfirst(substr($callingMethod, 3, strlen($callingMethod)));
        $this->matrixVal[] = [ $f, $v, getPdoVarType($v) ];
    }


}

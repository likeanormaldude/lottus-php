<?php

/**
 * Element Input Frame Component Class
 * */
class InputFrameComponent{

    private $inputFrameValues = [];
    private $displayClearSequenceIcon = TRUE;
    private $disableInputs = FALSE;
    private $coloredElements = TRUE;

    /**
     * @var string|null $appendedInputFrameClasses
     * */
    private $appendedInputFrameClasses = NULL;

    private string $style = 'input-frame-component';

    /**
     * @var int $componentCounter Increases every time the row of
     * the component changes. <p> The row is a new
     * subset of all small HTML components that are part of
     * the layout configuration of this object.
     * */
    private $componentCounter = 0;

    /**
     * Counts the input frames that had been built.
     * */
    private $inputFrameCounter = 0;

    private string $outputHTML = '';

    /**
     * @var array $args An associative array to be assigned on the respective component
     * */
    private array $args;

    /**
     * @var InputFrameComponent $SingleTon
     * */
    private static $SingleTon;

    public function __construct( array $args = [] ){
        $this->args = $args;
    }

    /**
     * @return InputFrameComponent
     */
    public static function getInstance( array $args = [] ){
        if(!self::$SingleTon)//Guarantee just one instance
            self::$SingleTon = new InputFrameComponent( $args );

        return self::$SingleTon;
    }

    public function setInputFrameCounter( int $ifc ){
        $this->inputFrameCounter = $ifc;
        return $this;
    }

    public function setArgs( array $args ){
        $this->args = $args;
        return $this;
    }

    public function resetComponent(){
        $this->outputHTML = '';
        $this->componentCounter = 0;
        $this->inputFrameCounter = 1;
        return $this;
    }

    /**
     * @param string $acif The class container all
     * the strings that must be appended to each input frame
     * */
    public function appendClassInputFrame( string $acif ){
        $this->appendedInputFrameClasses = $acif;
        return $this;
    }

    public function makeComponent(){

        // Reset previous component settings

        $this->_makeInputFrameElements();
        return $this->outputHTML;
    }

    public function getOutputHTML(){
        return $this->outputHTML;
    }

    private function _getIframeClasses(){
        // Dft class
        $arrC = ["frame-element"];

        // Append by class settings
        if( $this->disableInputs ) $arrC[] = "disabledInputFrame";
        if( $this->coloredElements ) $arrC[] = "element-text-input-frames";
        if( $this->appendedInputFrameClasses ) $arrC[] = $this->appendedInputFrameClasses;

        return join( ' ', $arrC );
    }

    /**
     * @param int $numOfFrames Desired number of input fields to print
     * @param int $numberOfElements Same role of $currRowNumber
     * @return string The number of input fields to be printed
     * */
    private function _getInputFrameCells( $numberOfElements ){
        $_html = '';

        if( $this->disableInputs ) $disabled = "disabled='disabled'";
        else $disabled = '';

        $classes = $this->_getIframeClasses();

        for( $i=0; $i < $numberOfElements; $i++ ){

            if( count( $this->inputFrameValues ) > 0 ){
                $valueAttr = " value='".$this->inputFrameValues[$this->componentCounter][$i]."' ";
            }else{
                $valueAttr = '';
            }

            $name = 'fr-elem'.
                strval(
                    ( ( $numberOfElements * $this->componentCounter ) + ( $this->inputFrameCounter + 1 ) )
                )
            ;

            $_html .= "
                <td>
                    <input 
                        type='text' 
                        class='".$classes."' 
                        name='".$name."' 
                        id='".$name."'
                        aria-rowindex='frame-row-".($this->componentCounter + 1)."'
                        ".$disabled." 
                        ".$valueAttr."
                    />
                </td>
            ";

            $this->inputFrameCounter++;
        }

        return $_html;
    }

    public function addStyle( string $s ){
        $this->style .= $s;
    }

    public function setStyle( string $style ){
        $this->style = $style;
    }

    public function marginVertical( int $v ){
        switch($v){
            case 1:
                $this->style .= ' my-1 ';
                break;
            case 2:
                $this->style .= ' my-2 ';
                break;
            case 3:
                $this->style .= ' my-3 ';
                break;
            case 4:
                $this->style .= ' my-4 ';
                break;
            case 5:
                $this->style .= ' my-5 ';
                break;

            default:
                $this->style .= '';
        }

        return $this;
    }

    /**
     * @param ComponentTitle|string $t
     * */
    private function _getComponentTitle( $t ){

        if( $t instanceof ComponentTitle ){
            return $t->getTitle();
        }else{
            return "<h4 id='frames-title".($this->componentCounter+1)."'>".$t."</h4>";
        }
    }

    /**
     * @param array $args
     * @return void Only modifies the outputHTML attribute
     * */
    private function _makeInputFrameElements(){
        for( $i=0; $i < $this->args['numberOfComponents']; $i++ ){

            if( isset( $this->args['args'][$this->componentCounter]['componentTitle'] ) ){
                $componentTitle = $this->_getComponentTitle(
                    $this->args['args'][$this->componentCounter]['componentTitle']
                );
            }else{
                $componentTitle = '';
            }

            if( isset( $this->args['args'][$i]['occurrences'] ) ){
                $paramOcc = $this->args['args'][$i]['occurrences'];
            }else{
                $paramOcc = NULL;
            }

            $htmlInputFrameCells = $this->_getInputFrameCells( $this->args['numberOfElements'] );
            $htmlOccurrencesCells = $this->_getOccurrencesCells( $paramOcc );
            $htmlClearSequenceIcon = $this->_getClearSequenceIcon();

            $this->outputHTML .= "
                <div class='".$this->style."'>
                    <div class='row container-title-elements justify-content-center'>
                        ".$componentTitle."
                    </div>
                    <div class='row justify-content-center'>
                        <table class='table-elements'>
                            <tr>
                                ".$htmlOccurrencesCells."
                                <td></td>
                            </tr>
                            <tr>
                                ".$htmlInputFrameCells."
                                <td>".$htmlClearSequenceIcon."</td>
                            </tr>
                        </table>
                    </div>
                </div>
            ";

            $this->componentCounter++;
        }
    }

    private function _getOccurrencesCells( array|NULL $collection ){
        $_html = '';

        if( $collection ){
            //        $_counter = 0;
            foreach( $collection as $v ){
//            $bg = ( ( $_counter %2 === 0 ) ? ' bg1 ' : ' bg2 ' );
                $_html .= "<td class='text-center'>".$v."</td>";
//            $_counter++;
            }
        }else{
            for( $i=0; $i < $this->args['numberOfElements']; $i++ ){
                $_html .= "<td class='text-center'></td>";
            }
        }

        return $_html;
    }

    public function displayClearSequenceIcon( bool $dcsi ){
        $this->displayClearSequenceIcon = $dcsi;
        return $this;
    }

    public function disableInputs( bool $di ){
        $this->disableInputs = $di;
        return $this;
    }

    private function _getClearSequenceIcon(){
        if( $this->displayClearSequenceIcon ){
            return "<i 
                id='icon-clear-elem".($this->componentCounter+1)."' 
                class='imoon-clear clear-elements-icon ml-3'
                ></i>
            ";
        }else{
            return "";
        }
    }

    public function assignInputFrameValuesByArray( array $inputFrameValues ){
        $this->inputFrameValues = $inputFrameValues;
        return $this;
    }

    public function coloredElements( bool $ce ){
        $this->coloredElements = $ce;
        return $this;
    }


}
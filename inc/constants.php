<?php

//__________________________GENERAL APP SETTINGS__________________________\\

/**
 * IMPORTANT!
 * Development Environment. Enables debug output on screen, for example.
 * */
define( "DEV_ENV", TRUE );
define('SITE', TRUE); //se false o site fica desativado

define( "APP_NAME", "lottus-php");
define( "HOME_URI", "http://".$_SERVER['HTTP_HOST']);

define( "ABS_HOME_URI", HOME_URI.'/'.APP_NAME);

define( 'ABSPATH', str_replace( '\\', '/', dirname(dirname(__FILE__)) ) );

define( "DEBUG_FILE_PATH", ABSPATH.'/tmp/debugHTML.html' );
//__________________________GENERAL APP SETTINGS__________________________//

//__________________________DATE AND TIME SETTINGS________________________\\
date_default_timezone_set('America/Sao_Paulo');
define("CURR_DATE", date('Y-m-d'));
define("CURR_DATETIME", date('Y-m-d H:i:s'));
//__________________________DATE AND TIME SETTINGS________________________//

//_________________________App General Error Messages_______________________\\
define('APP_ERR_500', 'Ocorreu um erro. Contate o administrador do sistema.');
//_________________________App General Error Messages_______________________\\

//__________________________Firebase Mega Sena Robot________________________\\
$firebaseKeys = [
  'mega-sena-818a2-7794ecd22e4e_NEW.json'
];
define(
     "KEY_FILE_MEGA_SENA_ROBOT_FILEPATH"
    ,ABSPATH.'/src/megaSenaRobotFirebase/'.$firebaseKeys[0]
);
//__________________________Firebase Mega Sena Robot________________________//








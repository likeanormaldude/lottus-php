<?php

//LOCALDB
define( "HOSTNAME", "localhost");
define( "DB_NAME", "lottus-php");
define( "DB_USER", "root");
define( "DB_PASSWORD", "");
define( "DB_CHARSET", "utf8");

//Debugging and Logging Settings
define( "DEBUG_ALL_QUERIES", FALSE); //record log of all queries
define( "DEBUG_ALL_INSERT_TRANSACTIONS", FALSE);
define( "DEBUG_ALL_UPDATE_TRANSACTIONS", FALSE);
define( "DEBUG_ALL_DELETE_TRANSACTIONS", FALSE);
define( "DEBUG", FALSE); //Used by PDO to display error messages

define( "DB_ERR_INSERT", 'Falha ao inserir os dados.');
define( "DB_ERR_SELECT", 'Falha ao selecionar os dados.');
define( "DB_ERR_UPDATE", 'Falha ao atualizar os dados.');
define( "DB_ERR_DELETE", 'Falha ao excluir os dados.');
define( "DB_ERR_QUERY", 'Falha ao executar comando no BD.');
define( "UKN_ERR_QUERY", 'Unknown error. Method: DB->query');
define( "DB_ERR_CONN", 'Falha ao conectar ao banco de dados.');

define( "LOG_ERROR", 'Falha ao gerar log.');

define( "DB_INSERT_OK", 'Dados cadastrados com sucesso.');
define( "DB_INSERT_ERROR", 'Desculpe, ocorreu um erro ao cadastrar. Contate o administrador.');
define( "DB_INSERT_FAILURE_SHORT", 'Falha ao inserir');

define( "DB_UPDATE_OK", 'Dados atualizados com sucesso.');
define( "DB_UPDATE_ERROR", 'Desculpe, ocorreu um erro ao editar os dados. Contate o administrador.');
define( "DB_UPDATE_FAILURE_SHORT", 'Falha ao editar');



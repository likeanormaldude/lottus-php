<?php

interface iLottusCache{
    /**
     * Writes the content according to database data
     * @param string $exc_msg
     * @return bool <p><b>TRUE</b> in case of successfully written cache or <b>FALSE</b> in case of
     * any exception or error.</p> <p>Note that when the return is <b>FALSE</b>,
     * <i>$exc_msg</i> will be set with the exception message.</p>
     * */
    public function feedCache( &$exc_msg = NULL ): bool;
}

<?php

interface iSingleTonModel{
    /**
     * @template T
     * @param T @Object
     * @return T
     * */
    public static function getInstance($Object);
}

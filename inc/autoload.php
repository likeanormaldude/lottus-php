<?php

function autoload($objectName){
    $directories = [ 'classes', 'models', 'interfaces'];
    foreach( $directories as $dir ){
        $filePathObject = ABSPATH . '/inc/'.$dir.'/'.$objectName.'.php';
        if( file_exists($filePathObject) ){
            require_once $filePathObject;
            break;
        }
    }
}

spl_autoload_register("autoload");

<?php

require_once ABSPATH."/inc/constants.php";

use TRegx\CleanRegex\Pattern;

// Resets debug file at every load during Development Environment
if( DEV_ENV ){
    $file = fopen( DEBUG_FILE_PATH, 'w');
    $write = fwrite($file, '');
    fclose($file);
    unset($file, $write);
}

function checkSession(){
    if( !isset( $_SESSION['idusuario'] ) )
        header('location: '.ABS_HOME_URI.'/index.php');
}

if(DEV_ENV){
    // Secondary Imports
    require_once ABSPATH.'/misc/Lottus/SessionManager.php';

    function setManualAppCachingConfig(){
        $SessionManager = SessionManager::getInstance();
        $SessionManager->setAppCachingProps([
            "precision" => 150
        ]);
        $SessionManager->createPrecisionFile();
        $SessionManager->updateOccurrenceFile();
    }

}


/**
 * It's a strip_tags enhanced. Adds a white space between words from different tags
 * @param string $string
 * @return string
 * */
function rip_tags($string){
    // ----- remove HTML TAGs -----
    $string = preg_replace ('/<[^>]*>/', ' ', $string);

    // ----- remove control characters -----
    $string = str_replace("\r", '', $string);    // --- replace with empty space
    $string = str_replace("\n", ' ', $string);   // --- replace with space
    $string = str_replace("\t", ' ', $string);   // --- replace with space

    // ----- remove multiple spaces -----
    $string = trim(preg_replace('/ {2,}/', ' ', $string));

    return $string;
}

function deleteDirectory($dir) {
    if (!file_exists($dir)) {
        return true;
    }

    if (!is_dir($dir)) {
        return unlink($dir);
    }

    foreach (scandir($dir) as $item) {
        if ($item == '.' || $item == '..') {
            continue;
        }

        if (!deleteDirectory($dir . DIRECTORY_SEPARATOR . $item)) {
            return false;
        }

    }

    return rmdir($dir);
}

function morre($v = 'Morreu'){
    if (is_array($v)) {
        echo "<pre>";
        print_r($v);
    } else {
        var_dump($v);
    }

    die();
}

/**
 * @param $var array|string|integer
 * @return bool
 */
function _empty( $var, $allowZeroAndFalse = TRUE ){
    //Check if it's array
    if( is_array( $var ) )
        return ( count( $var ) === 0 );

    if( !$allowZeroAndFalse ){
        return ( ( !!$var ) === FALSE );
    }else{
        return is_null( $var );
    }

}

function get_next_key_array($array,$key){
    $keys       = array_keys($array);
    $nextKey    = NULL;
    $position   = array_search($key, $keys);

    if (isset($keys[$position + 1]))
        $nextKey = $keys[$position + 1];

    return $nextKey;
}

/**
 * Iterates template paths to add to Twig Loader
 * @throws Exception
 * @return void
 * */
function includeTwigPaths( array $paths, Twig\Loader\FilesystemLoader &$loader ) : void{
    if( count( $paths ) > 0 )
        foreach( $paths as $path ) $loader->addPath( $path );
    else
        throw new Exception( '\$loader must not be empty' );
}

function getAppConstants(){
    return [
         'SITE'             => SITE
        ,'APP_NAME'         => APP_NAME
        ,'HOME_URI'         => HOME_URI
        ,'ABS_HOME_URI'     => ABS_HOME_URI
        ,'ABSPATH'          => ABSPATH
        ,'CURR_DATE'        => CURR_DATE
        ,'CURR_DATETIME'    => CURR_DATETIME
    ];
}

/**
 * @param array $sequenceSet The sequence set: <p>
 * @param array $occurrencesMatrix Whats is cached at <i>
 * ./cache/gameData/occurrences.json</i>
 * @return array <p>[
 * <br>&nbsp;&nbsp;&nbsp;'raw' => [
 * <br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 'occurrences' => [2, 1, 0, 4, 6, 3]
 * <br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 'elements' => [8, 7, 10, 12, 40, 41]
 * <br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 'score' => 16
 * <br>&nbsp;&nbsp;&nbsp;],
 * <br>&nbsp;&nbsp;&nbsp;'padded_str' => [
 * <br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 'occurrences' => ["(02x)", "(01x)", "(0x)", "(04x)", "(06x)", "(03x)"]
 * <br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 'elements' => ["08", "07", "10", "12", "40", "41"]
 * <br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 'score' => 16
 * <br>&nbsp;&nbsp;&nbsp;],
 * <br>]
 * */
function getSequenceSetDetails( array $sequenceSet, array $occurrencesMatrix ){
    $r = [];
    $score = 0;

    foreach( $sequenceSet as $elem ){
        if( isset( $occurrencesMatrix[(int)$elem] ) ){
            $elemOccLabel   = $occurrencesMatrix[(int)$elem]; //(12x)
            $patternInt     = '[^\d]+';
            $pInteger       = Pattern::of( $patternInt );
            $rs             = (int)$pInteger->replace( $elemOccLabel )->all()->with('');
            $elemOcc        = $rs;
        }else{
            $elemOcc = 0;
        }

        $score += $elemOcc;

        // Raw
        $r['raw']['occurrences'][] = $elemOcc;
        $r['raw']['elements'][] = $elem;

        // Padded String
        $r['padded_str']['occurrences'][] = '('.str_pad(
             strval( $elemOcc )
            ,2
            ,'0'
            ,STR_PAD_LEFT
        ).'x)';

        $r['padded_str']['elements'][] = str_pad( strval( $elem ), 2, '0', STR_PAD_LEFT );

    }

    $r['raw']['score'] = $score;
    $r['padded_str']['score'] = $score;

    return $r;
}
